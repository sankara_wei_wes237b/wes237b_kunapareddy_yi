#include <stdio.h>
#include "platform.h"
// #include "xgpio.h"
// #include "xuartps.h"
#include "xbasic_types.h"
#include "xtime_l.h"
#include "xil_io.h"
#include "xil_cache.h"
#include "xil_misc_psreset_api.h"

//CSR Commands
#define CSR_CMD_PWR 2
#define CSR_CMD_PRD 3
#define CSR_CMD_PRW 4

//CSR Address Map
#define CSR_addr_base 0x43C00000
#define CSR_off_sts 0x00000000
#define CSR_off_len 0x00000004
#define CSR_off_src 0x00000008
#define CSR_off_dst 0x0000000C
#define CSR_off_cmd 0x00000010
#define CSR_off_tmo 0x00000014
#define CSR_off_tmr 0x00000018
#define CSR_off_err 0x0000001C

//OCM Address Map
#define OCM_PSIZE 49152
#define OCM0_base 0x00000000
#define OCM1_base 0x0000C000
#define OCM2_base 0x00018000
#define OCM3_base 0x00024000

//DRAM Address Map
#define DDR_in_base 0x10000000
#define DDR_out_base 0x16000000
#define DDR_block_size 1876  //number of OCM_PSIZE to read

//TLC RW Intf
#define OCM_WRITE(BASE,OFF,DATA) *((Xuint32 *) ((BASE)+(OFF))) = (Xuint32) (DATA)
#define OCM_READ(BASE,OFF) (Xuint32) *((Xuint32 *) ((BASE)+(OFF)))
#define PL_WRITE(BASE,OFF,DATA) Xil_Out32((BASE)+(OFF),(u32)(DATA))
#define PL_READ(BASE,OFF) Xil_In32((BASE)+(OFF))
//DDR read/write
#define DDR_WRITE(BASE,OFF,DATA) *((Xuint32 *) ((BASE)+(OFF))) = (Xuint32) (DATA)
#define DDR_READ(BASE,OFF) (Xuint32) *((Xuint32 *) ((BASE)+(OFF)))
/*
 Can use contiguous OCM by setting these registers:
 sclr.OCM_CFG[RAM_HI] = 1F
 mpcore.SCU_CONTROL_REGISTER[Address_filtering_enable] 1
 mpcore.Filtering_Start_Address_Register = 0x0010_0000
 mpcore.Filtering_End_Address_Register = 0xFFE0_0000

 #define OCM_base = 0xFFFC0000;
 */

void printregs(){

	printf("\n\n\rsts: %#010x\n\r", Xil_In32(0x43c00000));
	printf("len: %lu\n\r", Xil_In32(0x43c00004));
	printf("src: %lu\n\r", Xil_In32(0x43c00008));
	printf("dst: %lu\n\r", Xil_In32(0x43c0000C));
	printf("cmd: %lu\n\r", Xil_In32(0x43c00010));
	printf("tmo: %lu\n\r", Xil_In32(0x43c00014));
	printf("tmr: %lu\n\r", Xil_In32(0x43c00018));
	printf("err: %lu\n\r", Xil_In32(0x43c0001C));

}

inline void write_ocm(Xuint32 ocm_base, Xuint32 ddr_off){
	Xuint32 ddr_in_off, i;
	ddr_in_off= ddr_off;
	Xuint32 input;
	for (i = 0; i < OCM_PSIZE; i += 4){
		input = DDR_READ(DDR_in_base, ddr_in_off);
		ddr_in_off += 4;
		OCM_WRITE(ocm_base, i, input);
//		if((ddr_in_off%640)<20 && (ddr_in_off% (640*480))<(640*5))
//			printf("%x ",input);
//		else if ((ddr_in_off% (640*480))==(640*5) || (ddr_in_off%640)==20&& (ddr_in_off% (640*480))<(640*5))
//			printf("\n\r");
	}
	Xil_DCacheFlushRange(ocm_base, OCM_PSIZE);
//	printf("write to ocm base: %x, ddr offset: %x, size %d, last 4 bytes: %x\n", ocm_base, ddr_off, OCM_PSIZE, input);
}

inline void read_ocm(Xuint32 ocm_base, Xuint32 ddr_off){
	Xuint32 ddr_out_off, i;
	ddr_out_off = ddr_off;
	Xuint32 output;

	Xil_DCacheInvalidateRange(ocm_base, OCM_PSIZE);
	for (i = 0; i < OCM_PSIZE; i += 4){
		output = OCM_READ(ocm_base, i);
		DDR_WRITE(DDR_out_base, ddr_out_off, output);
		ddr_out_off +=4;
//		if(i < 20 && ddr_off < 4*OCM_PSIZE)
//			printf("%x\n\r", output);
//		if((ddr_out_off%640)<20 && (ddr_out_off% (640*480))<(640*5))
//			printf("%x ",output);
//		else if ((ddr_out_off% (640*480))==(640*5) || (ddr_out_off%640)==20&& (ddr_out_off% (640*480))<(640*5))
//			printf("\n\r");
	}
	//ocm_base = PL_READ(CSR_addr_base, CSR_off_dst);
//	printf("read from ocm base: %x, ddr offset: %x, size %d, last 4 bytes: %x\n", ocm_base, ddr_off, OCM_PSIZE, output);
}


int main() {

	Xuint32 ddr_in_off, ddr_out_off;
	ddr_in_off = ddr_out_off = 0;


	//int ocm_src = 1; // 0 for OCM0 and 1 for OCM1
	//int ocm_dst = 1; // 0 for OCM2 and 1 for OCM3
	Xuint32 ocm_src = 1; // 0 for OCM0 and 1 for OCM1
	Xuint32 ocm_dst = 1; // 0 for OCM2 and 1 for OCM3

	int block_written = 0;


	//static Xuint32 out_arr[OCM_PSIZE / 4];

	XTime ts, te, tt;

	init_platform();

	printf("\n\n\rBeginning test...\n\r");
	printf("init cmd status: %x\n",PL_READ(CSR_addr_base,CSR_off_sts));

	XTime_GetTime(&ts);

	//write first block from DDR to OCM
    write_ocm(ocm_src?(OCM0_base):(OCM1_base), ddr_in_off);
    ddr_in_off += OCM_PSIZE;
    //printf("after write ocm cmd status: %x\n",PL_READ(CSR_addr_base,CSR_off_sts));
	//2. Write length, srcaddr, dstaddr
	PL_WRITE(CSR_addr_base, CSR_off_tmo, 0xFFFFFFFF);
	PL_WRITE(CSR_addr_base, CSR_off_src, ocm_src?(OCM0_base):(OCM1_base));
	PL_WRITE(CSR_addr_base, CSR_off_dst, ocm_dst?(OCM2_base):(OCM3_base));
	PL_WRITE(CSR_addr_base, CSR_off_len, OCM_PSIZE);
	//printf("before issue cmd status: %x\n",PL_READ(CSR_addr_base,CSR_off_sts));
	//3. Write command
	PL_WRITE(CSR_addr_base, CSR_off_cmd, CSR_CMD_PRW);
	//printf("right after issue cmd status: %x\n",PL_READ(CSR_addr_base,CSR_off_sts));
	write_ocm(ocm_src?(OCM1_base):(OCM0_base), ddr_in_off);
	ddr_in_off += OCM_PSIZE;
	//printf("after second write ocm cmd status: %x\n",PL_READ(CSR_addr_base,CSR_off_sts));
	//printf("\nfirst write and read done\n\n");
	while (block_written < DDR_block_size) {
		//printf("enter wile loop, block written %d\n", block_written);
		//printf("cmd status: %x\n",PL_READ(CSR_addr_base,CSR_off_sts));
		if (PL_READ(CSR_addr_base,CSR_off_sts) & (1<<8)){  //check the write_done bit
			ocm_src = !ocm_src;
			PL_WRITE(CSR_addr_base, CSR_off_tmo, 0xFFFFFFFF);
			PL_WRITE(CSR_addr_base, CSR_off_src, ocm_src?(OCM0_base):(OCM1_base));
			//PL_WRITE(CSR_addr_base, CSR_off_dst, ocm_dst?(OCM2_base):(OCM3_base));
			PL_WRITE(CSR_addr_base, CSR_off_len, OCM_PSIZE);
			//3. Write command
			PL_WRITE(CSR_addr_base, CSR_off_cmd, CSR_CMD_PWR);

			write_ocm(ocm_src?(OCM1_base):(OCM0_base), ddr_in_off);
			ddr_in_off += OCM_PSIZE;
			block_written++;
		}
		if (PL_READ(CSR_addr_base,CSR_off_sts) & (1<<12)){  //check the read_done bit
			ocm_dst = !ocm_dst;
			PL_WRITE(CSR_addr_base, CSR_off_tmo, 0xFFFFFFFF);
			//PL_WRITE(CSR_addr_base, CSR_off_src, ocm_src?(OCM0_base):(OCM1_base));
			PL_WRITE(CSR_addr_base, CSR_off_dst, ocm_dst?(OCM2_base):(OCM3_base));
			PL_WRITE(CSR_addr_base, CSR_off_len, OCM_PSIZE);
			//3. Write command
			PL_WRITE(CSR_addr_base, CSR_off_cmd, CSR_CMD_PRD);
			//printf("dst: %lu\n\r", Xil_In32(0x43c0000C));

			read_ocm(ocm_dst?(OCM3_base):(OCM2_base), ddr_out_off);
			ddr_out_off += OCM_PSIZE;

		}
	}

	XTime_GetTime(&te);

	//printf("Time to transfer: %08llu cycles, ", (te - ts));
	printf("Time to transfer: %08llu cycles, %06.2fus\n\r", 2 * (te - ts),
			((double) 1000000) * ((double) (te-ts)) / ((double)COUNTS_PER_SECOND));
/*
	for(j=0; j<1000; j++) {

		XTime_GetTime(&ts);

		///////////////
		// Xfer data //
		///////////////

		//1. Write data to OCM
		for (i = 0; i < OCM_PSIZE; i += 4)
			OCM_WRITE(OCM1_base, i, j);
		Xil_DCacheFlushRange(OCM1_base, OCM_PSIZE);

		//2. Write length, srcaddr, dstaddr
		PL_WRITE(CSR_addr_base, CSR_off_tmo, 0xFFFFFFFF);
		PL_WRITE(CSR_addr_base, CSR_off_src, OCM1_base);
		PL_WRITE(CSR_addr_base, CSR_off_dst, OCM2_base);
		PL_WRITE(CSR_addr_base, CSR_off_len, OCM_PSIZE);

		//3. Write command
		PL_WRITE(CSR_addr_base, CSR_off_cmd, CSR_CMD_PRW);

		//4. Read Status until Idle
		while ((PL_READ(CSR_addr_base,CSR_off_sts) & 0x1) != 0x1);

		//5. Read OCM
		Xil_DCacheInvalidateRange(OCM2_base, OCM_PSIZE);
		for (i = 0; i < OCM_PSIZE / 4; i++)
			out_arr[i] = OCM_READ(OCM2_base, i * 4);

		XTime_GetTime(&te);

		printf("Time to transfer-%03u: %08llu cycles, %06.2fus\n\r", out_arr[0], 2 * (te - ts),
				((double) 1000000) * ((double) (te-ts)) / ((double)COUNTS_PER_SECOND));

    }
   */

	printregs();

    cleanup_platform();
	return 0;
}


//To wait one second....
/*
XTime_GetTime(&te);
XTime_GetTime(&tt);
while((tt-te) < (COUNTS_PER_SECOND)) {
	XTime_GetTime(&tt);
}*/


//To print the output array...
/*for (i = 0; i < OCM_PSIZE / 4; i += 16) {
	xil_printf("\n\r");
	for (j = 0; j < 16; j++) {
		xil_printf("%05lu, ", out_arr[i + j]);
	}
}
xil_printf("\n\r");*/
