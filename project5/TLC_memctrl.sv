`include "TLC.h"

module TLC_memctrl(
   input clk
  ,input reset
  ,tlc_if.ctrl memctrl
  ,cmd_if.memctrl cmd_bus
  ,axi4s_if.master data_out
  ,axi4s_if.slave data_in
);

  reg cmd_recv, cmd_done;
  assign cmd_bus.cmd_recv = cmd_recv;
  assign cmd_bus.cmd_done = cmd_done;
                         
  /////////////////////////////////////
  // INSERT YOUR VARIABLES HERE      //
  /////////////////////////////////////
  reg [31:0] data_in_count, data_out_count;
  reg read_done, write_done;
  wire data_out_ready, data_out_valid;
  /////////////////////////////////////
  
  ///////////////////////////////////////
  // Modify this to read "sts" reg     //
  //   from software application.      //
  // Note: bit 0 is reserved.          //
  ///////////////////////////////////////
  assign cmd_bus.status = {3'b0, data_in.valid, 
                          3'b0, data_in.ready,
                          3'b0, data_out.valid,
                          3'b0, data_out.ready,
                          3'b0, read_done,
                          3'b0, write_done,
                          8'b0};
  ///////////////////////////////////////
  
  /////////////////////////////////////
  // Add the SOBEL instance here     //
  /////////////////////////////////////
   wire reset_n;
   assign reset_n = ~reset;
   sobel mysobel(
   .ap_clk(clk),
   .ap_rst_n(reset_n),
   .input_image_V_TDATA(data_in.data),
   .input_image_V_TVALID(data_in.valid),
   .input_image_V_TREADY(data_in.ready),
   .output_image_V_TDATA(data_out.data),
   .output_image_V_TVALID(data_out_valid),
   .output_image_V_TREADY(data_out_ready)
   );

   assign data_out.valid = data_out_valid & (~read_done);
   assign data_out_ready = data_out.ready & (~read_done);
  //////////////////////////////////////
  
  always_ff @(posedge clk)
  begin
    if(reset) begin
      cmd_recv <= 0;
      cmd_done <= 0;
      
      //////////////////////////
      // Reset your regs here //
      //////////////////////////
      data_in_count <= 32'b0;
      data_out_count <= 32'b0;
      read_done <=1;
      write_done <= 1;
      //////////////////////////
      
    end
    else 
    begin
    
      if(cmd_bus.cmd_valid)
        cmd_recv <= 1;
      else 
        cmd_recv <= 0;
       
      if(cmd_recv)
        cmd_done <= 1;
      else
        cmd_done <= 0;
        
      ////////////////////////////
      // Insert your code here  //
      ////////////////////////////
      if(cmd_recv && !cmd_done) begin
      
 /*      case(cmd_bus.command)
         `CSR_CMD_PWR: begin 
           data_in_count <=cmd_bus.length;
           read_done =0;
           if(!write_done && data_out.valid && data_out.ready) begin
             if (data_out_count == 1)
               write_done <=1;
             data_out_count <= data_in_count -1;
           end
         end 
         `CSR_CMD_PRD: begin 
           data_out_count <=cmd_bus.length;
           write_done = 0;
           if(!read_done && data_in.valid && data_in.ready) begin
             if(data_in_count == 1)
               read_done <= 1;
             data_in_count <= data_in_count -1;
           end
         end 
         `CSR_CMD_PRW: begin
           data_in_count <=cmd_bus.length;
           read_done =0;
           data_out_count <=cmd_bus.length;
           write_done = 0;
       
         end
 */
        if (cmd_bus.command==2) begin  //PS write to PL
          data_in_count <=cmd_bus.length;
          write_done =0;
          if(!read_done && data_out.valid && data_out.ready) begin
            if (data_out_count == 1)
              read_done <=1;
            data_out_count <= data_in_count -1;
          end
        end
        else if (cmd_bus.command==3) begin
          data_out_count <=cmd_bus.length;
          read_done = 0;
          if(!write_done && data_in.valid && data_in.ready) begin
            if(data_in_count == 1)
              write_done <= 1;
            data_in_count <= data_in_count -1;
          end
        end
        else if (cmd_bus.command==4) begin
        data_out_count <=cmd_bus.length;
        write_done = 0;      
        data_in_count <=cmd_bus.length;
        read_done =0;  
        end
 
      end
      else begin
        if(!write_done && data_in.valid && data_in.ready) begin
          if(data_in_count == 1)
            write_done <= 1;
          data_in_count <= data_in_count -1;
        end
      
        if(!read_done && data_out.valid && data_out.ready) begin
           if (data_out_count == 1)
             read_done <=1;
           data_out_count <= data_in_count -1;
        end
 
      end  
      ////////////////////////////
      
    end
  end
  
endmodule