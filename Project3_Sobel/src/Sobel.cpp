//
//  main.cpp
//  Lab3
//
//

#include "main.h"
#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    cout << "WES237B lab 3\n";

    VideoCapture cap(INPUT);

    Mat frame, gray, s_x, s_y, sobel_out, g_out;

	//init for gaussian denoise and sobel filter
	sobel_out = Mat(HEIGHT, WIDTH, CV_32FC1);
	g_out = Mat(HEIGHT, WIDTH, CV_32FC1);
	kernel_scale();

    char key=0;
       
    while(key != 'q'){
        
        cap >> frame;
	if(frame.empty()){
	    break;
	}
    
	cvtColor(frame, gray, CV_BGR2GRAY);
	resize(gray,gray,Size(WIDTH,HEIGHT));
    gray.convertTo(gray,CV_32FC1, 1/255.0);


	student_gaussian_denoise(gray, &g_out);
	student_sobel(g_out, &sobel_out);

	//show original gray image
	//show sobel result

     imshow("original", gray);
	 imshow("gaussian smooth", g_out);
     imshow("sobel", sobel_out);
     //key=waitKey
     key = waitKey(1);
     //key = 'q';
        
    }

    return 0;
}

