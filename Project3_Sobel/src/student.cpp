#include "main.h"
#include <stdio.h>
#include <math.h>

float kernel[5][5] = {{2, 4, 5, 4, 2},
					{4, 9, 12, 9, 4},
					{5, 12, 15, 12, 5},
					{4, 9, 12, 9, 4},
					{2, 4, 5, 4, 2}};
float kernelx[3][3] = {{-1, 0, 1},
                           {-2, 0, 2},
                           {-1, 0, 1}};
float kernely[3][3] = {{-1, -2, -1},
                           {0, 0, 0},
                           {1, 2, 1}};

void kernel_scale(void){
	for (int i=0; i<5; i++)
		for(int j=0; j<5; j++)
			kernel[i][j] /= 159;
}


void student_gaussian_denoise(Mat in, Mat* out){



	//*out = Mat(HEIGHT, WIDTH, CV_32FC1);
	for (int x=0; x<HEIGHT; x++){
		for(int y=0; y<WIDTH; y++) {
			out->at<float>(x, y) = 0;
			for(int off_row=-2; off_row<=2; off_row++) {
				for(int off_col=-2; off_col<=2; off_col++){
					if (x+off_row>=0 && x+off_row<HEIGHT && y+off_col>=0 && y+off_col<WIDTH)
						out->at<float>(x,y) += in.at<float>(x+off_row, y+off_col) * kernel[off_row+2][off_col+2];
				}
			}
		}
	}

}

void student_sobel(Mat in, Mat* out){


    //*out = Mat(HEIGHT, WIDTH, CV_32FC1);
	float Gx, Gy;
    for (int i=1; i<HEIGHT-1; i++) {
        for (int j=1; j<WIDTH-1; j++) {
            Gx = 0.0;
            Gy = 0.0;
            for(int off_row=-1; off_row<2; off_row++) {
                for (int off_col=-1; off_col<2; off_col++) {
                    Gx += in.at<float>(i+off_row, j+off_col) * kernelx[off_row+1][off_col+1];
                    Gy += in.at<float>(i+off_row, j+off_col) * kernely[off_row+1][off_col+1];
                }
            }
            out->at<float>(i,j) = sqrt(pow(Gx,2) + pow(Gy,2));
        }
    }

}
