//
//  main.cpp
//  Lab3
//
//


#include "main.h"
#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    cout << "WES237B lab 3\n";

    VideoCapture cap(INPUT);

    Mat frame, gray, s_x, s_y, sobel_out;
    char key=0;
       
    while(key != 'q'){
        
        cap >> frame;
	if(frame.empty()){
	    break;
	}
    
	cvtColor(frame, gray, CV_BGR2GRAY);
	resize(gray,gray,Size(WIDTH,HEIGHT));
    gray.convertTo(gray,CV_32FC1, 1/255.0);
    /*printf("gray points \n");
    for (int i = 0; i < HEIGHT; i++)
	    for(int j=0; j< WIDTH;j++)
    	    printf("%f ", gray.at<float>(0,i));
    printf("\n");
	//convert image type to CV_32FC1
*/

     /// Gradient X
     //Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
     Sobel( gray, s_x, CV_32FC1, 1, 0, 3, 1, 0, BORDER_DEFAULT );
     //convertScaleAbs( s_x, s_x );

     /*printf("s_x points \n");
     for (int i = 0; i < HEIGHT; i++)
    	 for(int j=0; j<WIDTH; j++)
     	     printf("%f ", s_x.at<float>(i,j));
     printf("\n");
*/

     Sobel( gray, s_y, CV_32FC1, 0, 1, 3, 1, 0, BORDER_DEFAULT );
     //convertScaleAbs( s_y, s_y );

     addWeighted( s_x, 0.5, s_y, 0.5, 0, sobel_out );
/*
     printf("sobel_out points \n");
     for (int i = 0; i < 8; i++)
     	printf("%f ", sobel_out.at<float>(0,i));
     printf("\n");
	*/
	//student_gaussian_denoise()
	//student_sobel()

	//show original gray image
	//show sobel result

     imshow("original", gray);
     imshow("sobel", sobel_out);
     //key=waitKey
     key = waitKey(1);
     //key = 'q';
        
    }

    return 0;
}

