//
//  main.h
//  Lab3
//
//

#ifndef main_h
#define main_h

#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

#define	CAM	0

#if CAM
#define INPUT	0
#define WIDTH   320
#define HEIGHT  256
#else
#define INPUT	"input.raw"
#define WIDTH   720
#define HEIGHT  480
#endif

//void student_sobel(Mat in, Mat* out);
//void student_gaussian_denoise(Mat in, Mat* out);
//void kernel_scale(void);

#endif /* main_h */
