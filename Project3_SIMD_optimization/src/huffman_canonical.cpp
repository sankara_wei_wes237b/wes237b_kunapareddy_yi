#include "huffman_canonical.h"

#include <stdio.h>

#include <string.h>

#define debug 0

#define MAXSIZE 345600
unsigned char decoded[MAXSIZE];
unsigned char encoded[MAXSIZE];

int freq[MAX_CHAR]; //storing frequency for each character
ecode code_map[MAX_CHAR];  //to be used by encoder
ecode code_map1[MAX_CHAR]; //to be used by decoder
huffnode *t_root;


int huffman_encode_canonical(const unsigned char *bufin,
           uint32_t bufinlen,
           unsigned char **pbufout,
           uint32_t *pbufoutlen) 
{
    //*pbufout=new unsigned char [bufinlen+100]; //allocating memory for out buf
    *pbufout = encoded;
	
	getFreqArr(bufin, bufinlen, freq); //go through input array and update freq array
    t_root=buildtree(freq);
    createcode(t_root, code_map); //create normal code_map
    convert_canonical_code(code_map);	//convert to canonical code
	
	unsigned int start=generateCanHeader(*pbufout, freq);  //write canonical header to out buf. (char and len pair)
    
    
	*pbufoutlen = encodeArr_Can(bufin, bufinlen, *pbufout, start, code_map);
 
	//remember to call delete[] before calling this function.
    return 0;
}

int huffman_decode_canonical(const unsigned char *bufin,
           uint32_t bufinlen,
           unsigned char **bufout,
           uint32_t *pbufoutlen)
{
        
        unsigned int start = bufin[0]*2+1;  //read canonical header and update the canonical code_map
		
		//find total char to for out buf allocation
		unsigned int total_ch, t1, t2, t3, t4;
		t1=bufin[start]; t2=bufin[start+1]; t3=bufin[start+2]; t4=bufin[start+3];
	    t2=t2<<8;
	    t3=t3<<16;
	    t4=t4<<24;
	    total_ch = t1 + t2 + t3 + t4;
		//*bufout = new unsigned char [total_ch];
        *bufout = decoded;
        
        *pbufoutlen=decodeArr_Can(bufin, bufinlen, *bufout);
        //*pbufoutlen=decodeArr(bufin, bufinlen, start, *bufout, t_root);
        return 0;

}


//all helper functions


#if 0
void getFreq(FILE *inf, int freq[])  //go through input file to retrieve freq of each character, save to array of size 256
{
	int c;	
	for (int i=0;i<MAX_CHAR;i++) 
		freq[i]=0;		
	while( (c=fgetc(inf)) != EOF){
		freq[c]++;
		
	}
}
#endif
void getFreqArr(const unsigned char *in, int len, int freq[])
{
	int c,i;	
	for (int i=0;i<MAX_CHAR;i++) 
		freq[i]=0;		
	for(i=0; i<len; i++){
		c=in[i];
		freq[c]++;
	}
}

#if 0
void generateTreeFile(FILE* outf, int freq[]) 
{
	int size=0;
	int total_ch=0;
	
	for (int i=0; i<256; i++) {
		if (freq[i]>0) {
			size++;
			total_ch += freq[i];
		}
	}
	putc(size, outf);
	printf("frequency table \nchar freq\n");
	for (int i=0; i<256; i++) {
		if (freq[i]>0){
			fprintf(outf, "%c%08x",i,freq[i]);
			printf("%c%8d\n",i,freq[i]);
		}
	}	
	//writing total num of char to be encoded.
	fprintf(outf, "%08x", total_ch);
	printf("unique char:%4d; total char:%4d\n",size,total_ch);
}

#endif
unsigned int generateTreeArr(unsigned char *out, int freq[])  //write tree info to char array, return index for writing actual data
{
	int size=0;
	unsigned int total_ch=0;
	int inx=0;
	
	for (int i=0; i<256; i++) {
		if (freq[i]>0) {
			size++;
			total_ch += freq[i];
		}
	}
	out[inx++]=size;
#if debug
	printf("frequency table \nchar freq\n");
#endif
	for (int i=0; i<256; i++) {
		if (freq[i]>0){
			unsigned int tmp=freq[i];
			out[inx++]=i;
			out[inx++]=tmp;
			out[inx++]=tmp>>8;
			out[inx++]=tmp>>16;
			out[inx++]=tmp>>24;
#if debug
			printf("%c%8d\n",i,freq[i]);
#endif
		}
	}	
	//writing total num of char to be encoded.
	out[inx++]=total_ch;
	out[inx++]=total_ch>>8;
	out[inx++]=total_ch>>16;
	out[inx++]=total_ch>>24;
#if debug	
	printf("unique char:%4d; total char:%4d\n",size,total_ch);
#endif
	return inx;
}



#if 0

void readTreeFile(FILE* inf, int freq[])
{
	int size;
	int count;
	char c;
	for (int i=0;i<MAX_CHAR;i++) 
		freq[i]=0;

	size = fgetc(inf);
	printf("unique char:%4d; freq table:\nchar freq\n",size);
	for (int i=0; i<size; i++) {
		fscanf(inf, "%c%08x", &c, &count);
		freq[c]=count;
	}
}
#endif
unsigned int readTreeArr(const unsigned char *in, int freq[]) //read array to retrieve the tree, return index of actual data
{
	int size;
	int count;
	int inx=0;
	unsigned int t1,t2,t3,t4;
	
	unsigned char c;
	for (int i=0;i<MAX_CHAR;i++) 
		freq[i]=0;

	size = in[inx++];
#if debug
	printf("unique char:%4d; freq table:\nchar freq\n",size);
#endif
	for (int i=0; i<size; i++) {
		c=in[inx++];
		t1=in[inx++];
		
		t2=in[inx++];
		t3=in[inx++];
		t4=in[inx++];
		t2=t2<<8;
		t3=t3<<16;
		t4=t4<<24;
		count=t1+t2+t3+t4;
		
		freq[c]=count;
#if debug
		printf("%c%8d\n",c,freq[c]);
		printf("%3d%3d%3d%3d%3d\n",c,t1,t2,t3,t4);
#endif
	}
	return inx;
}

void swap_node(huffnode *x, huffnode *y)
{
	int freq;
	char name;
	huffnode *left;
	huffnode *right;
	freq = x->freq;
	name = x->name;
	left = x->left;
	right = x->right;
	
	x->freq = y->freq;
	x->name = y->name;
	x->left = y->left;
	x->right = y->right;
	
	y->freq = freq;
	y->name = name;
	y->left = left;
	y->right = right;
}

void sort_queue(huffnode queue[], int len) {
	int i,j;
	for (i=0; i<len-1; i++) {
		for(j=i+1; j<len; j++) {
			if (queue[i].freq > queue[j].freq)
				swap_node(&queue[i], &queue[j]);
		}
	}
}

//external variable definition for tree nodes queue
huffnode first_queue[256];  //global variable to store tree nodes
huffnode second_queue[256]; //global variable to store tree nodes
int first_p1=0, first_p2=0, second_p1=0,second_p2=0; //global variable as pointers for first_queue and second queue

int chooseSecondQueue(void)  //determine whether pick next node from second queue.
{
	if (second_p1 <= second_p2)
		return 0;
	if (first_p1 <= first_p2)
		return 1;
	if (first_queue[first_p2].freq ==0 || second_queue[second_p2].freq == 0)
	{
		printf("error: node contain zero frequency. ");
		return 2;
	}
	if (first_queue[first_p2].freq > second_queue[second_p2].freq)
		return 1;
	else
		return 0;
}

huffnode *buildtree(int freq[]) //build the tree based on the freq array, return the pointer of tree root
{
	
	huffnode *p_left;
	huffnode *p_right;
	huffnode *t_root;
	first_p1=0; first_p2=0;second_p1=0;second_p2=0; //restoring huffnode queue pointers
	for(int i=0; i<256; i++)
	{
		if(freq[i]>0) {
			first_queue[first_p1].name = i;
			first_queue[first_p1].freq = freq[i];
			first_queue[first_p1].left = NULL;
			first_queue[first_p1].right = NULL;
			first_p1++;
		}
	}
	sort_queue(first_queue, first_p1);
	
	while(first_p2 < first_p1 || second_p2 < second_p1-1)
	{
		p_left = chooseSecondQueue()? &second_queue[second_p2++] : &first_queue[first_p2++];
		p_right = chooseSecondQueue()? &second_queue[second_p2++] : &first_queue[first_p2++];
		second_queue[second_p1].left = p_left;
		second_queue[second_p1].right = p_right;
		second_queue[second_p1].name = 0;
		second_queue[second_p1].freq = p_left->freq + p_right->freq;
		second_p1++;
	}
	t_root = &second_queue[second_p1-1];
	
	return t_root;

}

void retrievecode(huffnode *node, ecode e_code[], int code, int len)  //using recursion to traverse entire tree
{
	int l,l_code,r_code;
	if(node->left == NULL && node->right == NULL) {
		e_code[node->name].len = len;
		e_code[node->name].code = code;
		return;
	}	
	l= len +1;
	l_code = code;
	r_code = code + (1<<len);
	if (node->left != NULL)
		retrievecode(node->left, e_code, l_code, l);
	if (node->right != NULL)
		retrievecode(node->right, e_code, r_code, l);
}

void createcode(huffnode *root, ecode code[]) //generate encoding table based on the tree. 
{
	for (int i=0; i<MAX_CHAR;i++) 
		code[i].len=0;
	retrievecode(root, code, 0, 0);
#if debug
	//print code map table
	printf("code map table\nchar len code");
	for (int i=0; i<MAX_CHAR; i++)
		if(code[i].len>0)
			printf("%c%4d%8d\n",i,code[i].len,code[i].code);
#endif
}

void encodeFile(FILE *inf, FILE *outf, ecode code[]) //generate encoded file based on ecode array
{
	int c, tcode,t_ch;
	int ch=0;
	int ch_len=0;
	while((c=fgetc(inf))!=EOF) {
		tcode = code[c].code;
		ch += (tcode<<ch_len);
		ch_len+=code[c].len;
		//printf("%c",c); //debug only
		while(ch_len>=8) {
			t_ch = ch & 255;
			fputc(t_ch, outf);
			//printf("%X",t_ch); //debug only
			ch = ch >> 8;
			ch_len -= 8;
		}
	}
	if(ch_len>0) 
		fputc(ch, outf); //wrting last byte
}
unsigned int encodeArr(const unsigned char *in, int len, unsigned char *out, int start, ecode code[]) //generate encoded data based on ecode array, return length of output array
{
	int c, tcode,t_ch;
	int ch=0;
	int ch_len=0;
	int in_inx, out_inx;
	out_inx=start;
	for(in_inx=0; in_inx < len; in_inx++) {
		c=in[in_inx];
		tcode = code[c].code;
		ch += (tcode<<ch_len);
		ch_len+=code[c].len;
		//printf("%c",c); //debug only
		while(ch_len>=8) {
			t_ch = ch & 255;
			out[out_inx++]=t_ch;
			//printf("%X",t_ch); //debug only
			ch = ch >> 8;
			ch_len -= 8;
		}
	}
	if(ch_len>0) 
		//fputc(ch, outf); //wrting last byte
		out[out_inx++]=ch;
	
	return out_inx;
}



#if 0
void decodeFile(FILE *inf, FILE *outf, huffnode *root) 
////decode file based on huffman tree
{
	int total_char, c, num=0;
	huffnode *tree;
	tree=root;
	fscanf(inf, "%08x", &total_char);
	printf("total char to be decoded: %d\n",total_char);
	while ((c=fgetc(inf))!=EOF)
	{
		//printf("%X",c);//debug only
		for (int i=0; i<8; i++) {
			tree = (c & (1<<i))? tree->right : tree->left;
			if(tree->left == NULL && tree->right == NULL) {
				fputc(tree->name, outf);
				//printf("%c",tree->name); //debug only
				tree = root;
				if (++num >= total_char){
					printf("reach total char:%d, returning\n",total_char);
					return;
				}
			}
		}
	}
}

#endif
unsigned int decodeArr(const unsigned char *in, int len, int start, unsigned char *out, huffnode* root) //decode input based on huffman tree, return length of output array
{
	int total_char, c, num=0;
	int in_inx, out_inx=0;
	int t1,t2,t3,t4;
	in_inx=start;
	
	
	huffnode *tree;
	tree=root;
	
	t1=in[in_inx++]; t2=in[in_inx++]; t3=in[in_inx++]; t4=in[in_inx++];
	t2=t2<<8;
	t3=t3<<16;
	t4=t4<<24;
	total_char = t1 + t2 + t3 + t4;
	
	//printf("total char to be decoded: %d\n",total_char);
	for(;in_inx<len;in_inx++)
	{
		c=in[in_inx];
		for (int i=0; i<8; i++) {
			tree = (c & (1<<i))? tree->right : tree->left;
			if(tree->left == NULL && tree->right == NULL) {
				//fputc(tree->name, outf);
				out[out_inx++]=tree->name;
				//printf("%c",tree->name); //debug only
				tree = root;
				if (++num >= total_char){
					printf("reach total char:%d, returning\n",total_char);
					return out_inx;
				}
			}
		}
	}
	return out_inx;
}

//new function for canonical huffman
unsigned char sym[256];
unsigned char sym_len[256];
int sym_ind;
void sort_sym_len(unsigned char sym[], unsigned char sym_len[], int len)
{
	int least;
	for (int i=0; i<len-1; i++){
		least=i;
		for(int j=i+1; j<len; j++){
			if((sym_len[j]<sym_len[least]) || (sym_len[j]==sym_len[least] && sym[j]<sym[least]))
				least=j;
		}
		if(least>i){
			unsigned char sym_t, len_t;
			sym_t=sym[i];
			len_t=sym_len[i];
			sym[i]=sym[least];
			sym_len[i]=sym_len[least];
			sym[least]=sym_t;
			sym_len[least]=len_t;
		}
	}
}
void convert_canonical_code(ecode code[])	//convert to canonical code
{
	sym_ind=0;
	for(unsigned int i=0; i<256; i++)
	{
		if(code[i].len>0){
			sym[sym_ind]=i;
			sym_len[sym_ind]=code[i].len;
			sym_ind++;
		}
	}
	sort_sym_len(sym,sym_len,sym_ind);
	int temp_code=0;
	int prev_len=0;
	for(int j=0; j<sym_ind; j++){
		temp_code = temp_code<<(sym_len[j]-prev_len);
		code[sym[j]].code=temp_code;
		//code[sym[j]].len=sym_len[j];
		if (code[sym[j]].len!=sym_len[j])
			printf("error: canonical len mismatch for char %c\n",sym[j]);
		prev_len=sym_len[j];
		temp_code++;
		
	}
#if debug
	printf("canonical code map table\nchar len code");
	for (int i=0; i<256; i++)
		if(code[i].len>0)
			printf("%c%4d%8d\n",i,code[i].len,code[i].code);
#endif
}


unsigned int generateCanHeader(unsigned char *out, int freq[])  //write canonical header to out buf. (char and len pair)
{
	int p=0;
	out[p++]=sym_ind;
	for (int i=0; i<sym_ind; i++){
		out[p++]=sym[i];
		out[p++]=sym_len[i];
	}
	unsigned int total_ch=0;
	for (int j=0; j<256; j++) {
		if (freq[j]>0) {
			total_ch += freq[j];
		}
	}

	//writing total num of char to be encoded.
	out[p++]=total_ch;
	out[p++]=total_ch>>8;
	out[p++]=total_ch>>16;
	out[p++]=total_ch>>24;
	
	return p;

}
unsigned int encodeArr_Can(const unsigned char *in, int len, unsigned char *out, int start, ecode code[]) //generate encoded data based on ecode array, return length of output array
{
	unsigned int c, tcode,t_ch;
	unsigned int ch=0;
	unsigned int ch_len=0;
	int in_inx, out_inx;
	out_inx=start;
	for(in_inx=0; in_inx < len; in_inx++) {
		c=in[in_inx];
		tcode = code[c].code;
		ch = (ch<<code[c].len)+tcode;
		ch_len+=code[c].len;
		//printf("%c",c); //debug only
		while(ch_len>=8) {
			ch_len -= 8;
			t_ch = ch>>ch_len;
			out[out_inx++]=t_ch;
			//printf("%X",t_ch); //debug only
			ch = ch - (t_ch<<ch_len);
		}
	}
	if(ch_len>0) 
		//fputc(ch, outf); //wrting last byte
		out[out_inx++]=ch<<(8-ch_len);
	
	return out_inx;
}



unsigned int decodeArr_Can(const unsigned char *in, int len, unsigned char *out)
{

	int p=0, out_inx=0, tlen;
	int header_size=in[p++];
	unsigned char c;
	unsigned char symbol[header_size];
	unsigned char n_code[header_size];
	for(int j=0;j<header_size;j++)
		n_code[j]=0;
	
	for (int i=0; i<header_size;i++){
		symbol[i]=in[p++];
		tlen=in[p++];
		n_code[tlen-1]++;
	}
#if debug
printf("symbol table\n");
for (int i=0; i<header_size;i++){
	printf("%c",symbol[i]);
	
}
printf("\n");
for (int i=0; i<header_size;i++){
	printf("%4d",n_code[i]);
	
}
printf("\n");
#endif

	unsigned int t1,t2,t3,t4, total_char;
	t1=in[p++]; t2=in[p++]; t3=in[p++]; t4=in[p++];
	t2=t2<<8;
	t3=t3<<16;
	t4=t4<<24;
	total_char = t1 + t2 + t3 + t4;
	
	int accumulator=0, first_code=0, first_index=0, code_len=0, nbit;
	for(;p<len;p++)
	{
		c=in[p];
#if debug
printf("\n%3x\n",c);
#endif
		for (int i=0;i<8;i++) {
			nbit=(c & (1<<(7-i)))>>(7-i);
			accumulator = accumulator*2 + nbit;
#if debug
printf("nbit%4d; acc%4d; first_code%4d; first_ind%4d; code_len%4d;",nbit,accumulator,first_code,first_index, code_len);
#endif
			first_code = first_code<<1;
			if(accumulator-first_code<n_code[code_len]){
				out[out_inx++]=symbol[accumulator-first_code+first_index];
				if(out_inx>=total_char){
					return out_inx;
				}
#if debug
printf("character: %c\n",out[out_inx-1]);
#endif
				code_len=0;
				accumulator=0;
				first_code=0;
				first_index=0;
				continue;
			}
					
			first_code+=n_code[code_len];
			first_index+=n_code[code_len];
			code_len++;
		}

	}
	return out_inx;	
	
}
