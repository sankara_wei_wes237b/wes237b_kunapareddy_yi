//
//  main.cpp
//  Lab2
//
//  Created by Alireza on 7/5/16.
//  Copyright © 2016 Alireza. All rights reserved.
//

#include "main.h"
#include "time.h"
#include <sys/time.h>
long long utime() {
  static struct timeval t;
  gettimeofday(&t, NULL);
  return (long long) (1e6 * t.tv_sec + t.tv_usec);
}

int main(int argc, const char * argv[]) {
    
    
    long long t;
    cout << "WES237B lab 3\n";

    VideoCapture cap(INPUT);
	cap.set(CV_CAP_PROP_FPS,60);
    cap.set(CV_CAP_PROP_FRAME_WIDTH,320);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT,240);

    Mat frame, gray;
    char key=0;
    
    encoder_init();
    decoder_init();
    
    time_t timer;
    
    time_t start;
    time(&start);
    
    int fps_cnt = 0;
       
    while(key != 'q'){
        
        cap >> frame;
	if(frame.empty()){
	    break;
	}

        time(&timer);
        if(timer - start > 1l) {
            start = timer;
            std::cout << "fps: " << fps_cnt << endl;
            fps_cnt = 0;
        }
        fps_cnt++;
        
        cvtColor(frame, gray, CV_BGR2GRAY);
        resize(gray, gray, Size(WIDTH, HEIGHT));
        // srand(utime());//sankar
       // t = utime();
        /* encode */
        vector<uchar> packet = encoder_job(gray);
        
        /* decode */
        Mat q_gray = decoder_job(packet);
      // printf("Time: %lld\n", utime() - t);
               
        imshow("received", q_gray);
#if CAM
        imshow("Cam", gray);
#endif
        key = waitKey(1);
        
        time(&timer);
        
    }

    return 0;
}
