#include "main.h"
#include <stdio.h>
#include "dct_student.h"
#include <math.h>
#include <arm_neon.h>
#include <omp.h>
#define PI 3.1415926

static float cosines[8][8] __attribute__((aligned(0x10)))=
   {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0}};

 static float coef[8][8] __attribute__((aligned(0x10))) =
   {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0}};
 static float c[8] = {0.7071, 1, 1, 1, 1, 1, 1, 1};
	
 static float dct2_coef[8][8] __attribute__((aligned(0x10)))=
   {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0}};
float c_vector[8][8] __attribute__((aligned(0x10)));

	
void init_dct2(void) {
    float ci, cj;
    for (int i=0; i<8; i++) {
        ci=i? 1.0:sqrt(0.5);
        for (int j=0; j<8; j++) {
            cj=j?1.0:sqrt(0.5);
            cosines[i][j]=cos((2*i+1)*j*PI/16);
            coef[i][j] = ci*cj;
        }
    }

    for (int k=0; k<8; k++) {
        for (int n=0; n<8; n++) {
            dct2_coef[k][n] = cos((n+0.5)*k*PI/8)/2;
        }
    }

    //create c_vector based on dct2_coef matrix to merge the c[j] portion
    for (int i=0; i<8; i++) {
        for(int j=0; j<8; j++) {
            c_vector[i][j] = dct2_coef[i][j];
            if (!i)
                c_vector[i][j] *= 0.7071;           
        }
    }

}





void DCT2_student(Mat input, Mat output)
{
#pragma omp critical  
  float temp = 0.0;
    //output = Mat(8, 8, CV_32FC1);
    float mat_t[8][8] __attribute__((aligned(0x10)));
	float in_t[8][8] __attribute__((aligned(0x10)));
    
    union {
    float32x4_t acc;
    float f[4];
    } u;
    

#pragma omp parallel for
    //copy input Mat data to in_t 
    for (int i=0; i<8; i++){ 
        //for(int j=0; j<8; j++) 
        in_t[i][0] = input.at<float>(i,0);
	in_t[i][1] = input.at<float>(i,1);
	in_t[i][2] = input.at<float>(i,2);
	in_t[i][3] = input.at<float>(i,3);
	in_t[i][4] = input.at<float>(i,4);
	in_t[i][5] = input.at<float>(i,5);
	in_t[i][6] = input.at<float>(i,6);
	in_t[i][7] = input.at<float>(i,7);
}
#pragma omp parallel for
    //1D DCT for all rows
    for (int i=0; i<8; i++) {
        for(int j=0; j<8; j++) {
            
            //u.acc = vdup_n_f32(0.f);
                //temp+= dct2_coef[j][n] * in_t[i][n] * c[j];
            u.acc = vmulq_f32( vld1q_f32(&c_vector[j][0]), vld1q_f32(&in_t[i][0]) );
            u.acc = vmlaq_f32( u.acc, vld1q_f32(&c_vector[j][4]), vld1q_f32(&in_t[i][4]) ); 
            mat_t[j][i]=u.f[0] + u.f[1] + u.f[2] + u.f[3];;
        }
    }
#pragma vector aligned
#pragma omp parallel for
    for (int i=0; i<8; i++) {
        for(int j=0; j<8; j++) {
            
            //u.acc = vdup_n_f32(0.f);
                //temp+= dct2_coef[j][n] * in_t[i][n] * c[j];
            u.acc = vmulq_f32( vld1q_f32(&c_vector[j][0]), vld1q_f32(&mat_t[i][0]) );
            u.acc = vmlaq_f32( u.acc, vld1q_f32(&c_vector[j][4]), vld1q_f32(&mat_t[i][4]) ); 
            output.at<float>(j,i)=u.f[0] + u.f[1] + u.f[2] + u.f[3];;
        }
    }   
    
}


void iDCT2_student(Mat input, Mat output)
{
#pragma omp critical
	//output = Mat(8, 8, CV_32FC1);
	//float mat_t[8][8];
    float mat_t[8][8] __attribute__((aligned(0x10)));

#pragma vector aligned
#pragma omp parallel for
	//1D iDCT for all rows
	for (int i=0; i<8; i++) {
             
		for(int j=0; j<8; j++) {
			float temp = 0.0;
			
				
				//temp+= dct2_coef[n][j] * input.at<float>(i,n) * c[n];
								
				temp+= dct2_coef[0][j] * input.at<float>(i,0) * c[0];
				temp+= dct2_coef[1][j] * input.at<float>(i,1) * c[1];
				temp+= dct2_coef[2][j] * input.at<float>(i,2) * c[2];
				temp+= dct2_coef[3][j] * input.at<float>(i,3) * c[3];
				temp+= dct2_coef[4][j] * input.at<float>(i,4) * c[4];
				temp+= dct2_coef[5][j] * input.at<float>(i,5) * c[5];
				temp+= dct2_coef[6][j] * input.at<float>(i,6) * c[6];
				temp+= dct2_coef[7][j] * input.at<float>(i,7) * c[7];
				//j++;
			
		
			mat_t[j][i]=temp;
		}
	}
#pragma vector aligned
#pragma omp parallel for	
	for (int i=0; i<8; i++) {
		
		for(int j=0; j<8; j++) {
			float temp = 0.0;
						
				temp+= dct2_coef[0][j] * mat_t[i][0] * c[0];
				temp+= dct2_coef[1][j] * mat_t[i][1] * c[1];
				temp+= dct2_coef[2][j] * mat_t[i][2] * c[2];
				temp+= dct2_coef[3][j] * mat_t[i][3] * c[3];
				temp+= dct2_coef[4][j] * mat_t[i][4] * c[4];
				temp+= dct2_coef[5][j] * mat_t[i][5] * c[5];
				temp+= dct2_coef[6][j] * mat_t[i][6] * c[6];
				temp+= dct2_coef[7][j] * mat_t[i][7] * c[7];
				
			
			output.at<float>(j,i) = temp;
		}
	}
}



