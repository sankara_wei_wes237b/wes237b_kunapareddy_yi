import sys
import numpy
import scipy

#print 'Number of arguments:',len(sys.argv),'arguments'
#print 'Argument list:',str(sys.argv)

inputfile=str(sys.argv[1])
outputfile=str(sys.argv[2])

f=scipy.fromfile(open(inputfile),dtype=scipy.float32)
numpy.savetxt(outputfile,f)
f.tofile(outputfile,sep="\n",format="%1f")
