#include <stdio.h>
#define MAX_CHAR 256

//type def for struct
typedef struct node huffnode;
typedef struct encode ecode;
struct node {
	int freq;
	char name;
	huffnode *left;
	huffnode *right;
};

struct encode {
	int code;
	int len;
};

//external variable declaration
extern huffnode first_queue[256];  //global variable to store tree nodes
extern huffnode second_queue[256]; //global variable to store tree nodes
extern int first_p1, first_p2, second_p1,second_p2; //global variable as pointers for first_queue and second queue

//functions declaration
void getFreq(FILE *inf, int freq[]);  //go through input file to retrieve freq of each character, save to array of size 256
void generateTreeFile(FILE *outf, int freq[]);
void readTreeFile(FILE *inf, int freq[]);




huffnode *buildtree(int freq[]); //build the tree based on the freq array, return the pointer of tree root

void createcode(huffnode *root, ecode code[]); //generate encoding table based on the tree
void encodeFile(FILE *inf, FILE *outf, ecode code[]); //generate encoded file based on ecode array
void decodeFile(FILE *inf, FILE *outf, huffnode* root); //decode file based on huffman tree
