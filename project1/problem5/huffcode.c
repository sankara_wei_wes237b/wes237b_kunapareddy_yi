#include <stdio.h>
#include "huff_f.h"
#include <string.h>

void main(int argc, char* argv[])
{
	if (argc<4) {
		printf("missing required arguments. \n");
		return;
	}
	FILE *inf;
	FILE *outf;
	
	int freq[MAX_CHAR]; //storing frequency for each character

	ecode code_map[MAX_CHAR];
	huffnode *t_root;
	
	if (!strcmp(argv[1],"e"))   //encoding
	{
		printf("huffman encoding file %s\n",argv[2]);
		int i=2;
		if((inf=fopen(argv[i],"r"))==NULL || (outf=fopen(argv[++i],"w"))==NULL ) {
			printf("can't open file %s\n",argv[i]);
			return;
		}
		
		getFreq(inf, freq); //go through input file and update freq array
		rewind(inf);
		generateTreeFile(outf, freq); //generating tree file based on freq array
		t_root=buildtree(freq);
		createcode(t_root, code_map);
		encodeFile(inf, outf, code_map);
		
	}
	else if (!strcmp(argv[1],"d"))   //decoding
	{
		printf("huffman decoding file %s\n",argv[2]);
		int i=2;
		if((inf=fopen(argv[i],"r"))==NULL || (outf=fopen(argv[++i],"w"))==NULL ) {
			printf("can't open file %s\n",argv[i]);
			return;
		}
		
		readTreeFile(inf, freq);
		t_root=buildtree(freq);
		decodeFile(inf, outf, t_root); // decode input file based on tree and write to output file
		
	}
	else {
		printf("invalid option %s \n",argv[1]);
		
	}
	
	fclose(inf);
	fclose(outf);
}
