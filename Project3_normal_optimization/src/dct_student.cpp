#include "main.h"
#include <stdio.h>
#include "dct_student.h"
#include <math.h>
#include <omp.h>
#define PI 3.1415926

float cosines[8][8] __attribute__((aligned(0x10)))=
   {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0}};

float coef[8][8] __attribute__((aligned(0x10))) =
   {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0}};
float c[8] __attribute__((aligned(0x10)))= {0.7071, 1, 1, 1, 1, 1, 1, 1};
	
 float dct2_coef[8][8] __attribute__((aligned(0x10)))=
   {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0}};


void init_dct2(void) {
	#pragma vector aligned
	for (int k=0; k<8; k++) {
		for (int n=0; n<8; n++) {
			dct2_coef[k][n] = cos((n+0.5)*k*PI/8)/2;
		}
	}

}
	


void DCT2_student(Mat input, Mat output)
{
#pragma omp critical
	
	//output = Mat(8, 8, CV_32FC1);
	float mat_t[8][8];
	//float mat_t[8][8] __attribute__((aligned(0x10)));

#pragma omp parallel for
	//1D DCT for all rows
	for (int i=0; i<8; i++) {
		for(int j=0; j<8; j++) {
			float temp = 0.0;
			//for(int n=0; n<8; n++) {
				
				//temp+= dct2_coef[j][n] * input.at<float>(i,n) * c[j];
				temp+= dct2_coef[j][0] * input.at<float>(i,0) * c[j];
				temp+= dct2_coef[j][1] * input.at<float>(i,1) * c[j];
				temp+= dct2_coef[j][2] * input.at<float>(i,2) * c[j];
				temp+= dct2_coef[j][3] * input.at<float>(i,3) * c[j];
				temp+= dct2_coef[j][4] * input.at<float>(i,4) * c[j];
				temp+= dct2_coef[j][5] * input.at<float>(i,5) * c[j];
				temp+= dct2_coef[j][6] * input.at<float>(i,6) * c[j];
				temp+= dct2_coef[j][7] * input.at<float>(i,7) * c[j];
				
			//}
		
			mat_t[j][i]=temp;
		}
	}
#pragma vector aligned	
#pragma omp parallel for
	for (int i=0; i<8; i++) {
		for(int j=0; j<8; j++) {
			float temp = 0.0;
			//for(int n=0; n<8; n++) {
				
				//temp+= dct2_coef[j][n] * mat_t[i][n] * c[j];
				temp+= dct2_coef[j][0] * mat_t[i][0] * c[j];
				temp+= dct2_coef[j][1] * mat_t[i][1] * c[j];
				temp+= dct2_coef[j][2] * mat_t[i][2] * c[j];
				temp+= dct2_coef[j][3] * mat_t[i][3] * c[j];
				temp+= dct2_coef[j][4] * mat_t[i][4] * c[j];
				temp+= dct2_coef[j][5] * mat_t[i][5] * c[j];
				temp+= dct2_coef[j][6] * mat_t[i][6] * c[j];
				temp+= dct2_coef[j][7] * mat_t[i][7] * c[j];
				
			//}
		
			output.at<float>(j,i) = temp;
		}
	}
}


void iDCT2_student(Mat input, Mat output)
{
#pragma omp critical
	//output = Mat(8, 8, CV_32FC1);
	float mat_t[8][8];
    //float mat_t[8][8] __attribute__((aligned(0x10)));

#pragma vector aligned
#pragma omp parallel for
	//1D iDCT for all rows
	for (int i=0; i<8; i++) {
             
		for(int j=0; j<8; j++) {
			float temp = 0.0;
			
				
				//temp+= dct2_coef[n][j] * input.at<float>(i,n) * c[n];
								
				temp+= dct2_coef[0][j] * input.at<float>(i,0) * c[0];
				temp+= dct2_coef[1][j] * input.at<float>(i,1) * c[1];
				temp+= dct2_coef[2][j] * input.at<float>(i,2) * c[2];
				temp+= dct2_coef[3][j] * input.at<float>(i,3) * c[3];
				temp+= dct2_coef[4][j] * input.at<float>(i,4) * c[4];
				temp+= dct2_coef[5][j] * input.at<float>(i,5) * c[5];
				temp+= dct2_coef[6][j] * input.at<float>(i,6) * c[6];
				temp+= dct2_coef[7][j] * input.at<float>(i,7) * c[7];
				//j++;
			
		
			mat_t[j][i]=temp;
		}
	}
#pragma vector aligned
#pragma omp parallel for	
	for (int i=0; i<8; i++) {
		
		for(int j=0; j<8; j++) {
			float temp = 0.0;
						
				temp+= dct2_coef[0][j] * mat_t[i][0] * c[0];
				temp+= dct2_coef[1][j] * mat_t[i][1] * c[1];
				temp+= dct2_coef[2][j] * mat_t[i][2] * c[2];
				temp+= dct2_coef[3][j] * mat_t[i][3] * c[3];
				temp+= dct2_coef[4][j] * mat_t[i][4] * c[4];
				temp+= dct2_coef[5][j] * mat_t[i][5] * c[5];
				temp+= dct2_coef[6][j] * mat_t[i][6] * c[6];
				temp+= dct2_coef[7][j] * mat_t[i][7] * c[7];
				
			
			output.at<float>(j,i) = temp;
		}
	}
}



