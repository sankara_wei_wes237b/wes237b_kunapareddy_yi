//
//  main.cpp
//  Lab2
//
//  Created by Alireza on 7/5/16.
//  Copyright © 2016 Alireza. All rights reserved.
//

#include "main.h"
#include "time.h"

int main(int argc, const char * argv[]) {
    
    cout << "WES237B lab 2\n";

    VideoCapture cap(INPUT);

    Mat frame, gray;
    char key=0;
    
    encoder_init();
    decoder_init();
    
    time_t timer;
    
    time_t start;
    time(&start);
    
    int fps_cnt = 0;
       
    while(key != 'q'){
        
        cap >> frame;
	if(frame.empty()){
	    break;
	}

        time(&timer);
        if(timer - start > 1l) {
            start = timer;
            std::cout << "fps: " << fps_cnt << endl;
            fps_cnt = 0;
        }
        fps_cnt++;
        
        cvtColor(frame, gray, CV_BGR2GRAY);
        resize(gray, gray, Size(WIDTH, HEIGHT));
        
        /* encode */
        vector<uchar> packet = encoder_job(gray);
        
        /* decode */
        Mat q_gray = decoder_job(packet);
               
        imshow("received", q_gray);
#if CAM
        imshow("Cam", gray);
#endif
        key = waitKey(1);
        
        time(&timer);
        
    }

    return 0;
}
