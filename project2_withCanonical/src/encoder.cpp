//
//  encoder.cpp
//  Lab2
//
//  Created by Alireza on 7/6/16.
//  Copyright © 2016 Alireza. All rights reserved.
//

#include "main.h"
#include <stdio.h>
#include "huffman_canonical.h"
#include "dct_student.h"

struct esv{
    uint startcode;
    uint16_t n_mbs;
	esv(){
		startcode = 0x000001BA;
	}
};

Mat quanMat_en;
vector<Mat> mbs_q_prev;

/* encoder initializer */
void encoder_init(){
    
    /* mpeg 1 quntizaton matrix */
    float q_arr[8][8] =
    {{8 , 16, 19, 22, 26, 27, 29, 34},
        {16, 16, 22, 24, 27, 29, 34, 37},
        {19, 22, 26, 27, 29, 34, 34, 38},
        {22, 22, 26, 27, 29, 34, 37, 40},
        {22, 26, 27, 29, 32, 35, 40, 48},
        {26, 27, 29, 32, 35, 40, 48, 58},
        {26, 27, 29, 34, 38, 46, 56, 69},
        {27, 29, 35, 38, 46, 56, 69, 83}};
    
    quanMat_en = Mat(mbh, mbw, CV_32FC1);
    for(int r=0;r<8;r++){
        for(int c=0;c<8;c++){
            quanMat_en.at<float>(r,c) = q_arr[r][c]/127.0;
        }
    }
    init_dct();

    int total_pixels = WIDTH * HEIGHT;
    int total_mbs = total_pixels/(mbh*mbw);
    Mat z = Mat::zeros(mbh, mbw, CV_32FC1);
    for(int i=0; i<total_mbs; i++){
        mbs_q_prev.push_back(z.clone());
    }
}

/* get micro blocks */
vector <Mat> get_mbs(Mat frame){
    vector <Mat> mbs;
    for (int r=0; r<frame.rows; r+=mbh){
        for(int c=0; c<frame.cols; c+=mbw){
            Mat mb = frame(Rect(c, r, mbw, mbh)).clone();
            mbs.push_back(mb);
        }
    }
    return mbs;
}

/* get DCT */
vector <Mat> get_DCT(vector<Mat> mbs){
    vector<Mat> mbs_dct;
    
    for(int i=0; i<mbs.size(); i++){
        Mat mb = mbs[i].clone();
        Mat mb_dct;
        //dct(mb, mb_dct);
        mb_dct=Mat(8, 8, CV_32FC1);
        DCT_student(mb, mb_dct);
        mbs_dct.push_back(mb_dct);
    }
    
    return mbs_dct;
}

/* quantization */
vector <Mat> get_quant(vector<Mat> mbs_dct){
    vector<Mat> mbs_q;
    for(int i=0; i<mbs_dct.size(); i++){
        Mat mb_dct = mbs_dct[i];
        Mat mb_q;
        divide(mb_dct, quanMat_en, mb_q);
    
        /* rounding */
        for(int r=0; r<mbh; r++){
            for(int c=0; c<mbw; c++){
                mb_q.at<float>(r,c) = round(mb_q.at<float>(r,c));
            }
        }
        
        mbs_q.push_back(mb_q);
    }
    return mbs_q;
}

/* compare to previous */
vector<iMat> compare_mbs(vector<Mat> mbs_q){
    vector<iMat> mbs_q_compd;
    for(int i=0; i<mbs_q.size(); i++){
        Mat mb_diff;
        absdiff(mbs_q[i], mbs_q_prev[i], mb_diff);
        Scalar s = sum(mb_diff);
        if (s[0] > 1.0){
            iMat temp;
            mbs_q[i].copyTo(temp.m);
            
            /* converting to 8 bits signed */
            temp.m.convertTo(temp.m, CV_8SC1);
            temp.i = i;
            mbs_q_compd.push_back(temp);
        }
        mbs_q[i].copyTo(mbs_q_prev[i]);
    }
    return mbs_q_compd;
}

/* to zigzag */
vector<iArr> to_zigzag( vector<iMat> mbs_q_compd ){
    
    vector<iArr> mbs_zigzag;
    
    for(int mbn=0; mbn<mbs_q_compd.size(); mbn++){
        
        int currDiag = 0;
        int loopFrom;
        int loopTo;
        int row;
        int col;
        
        iArr mb_zigzag;
        mb_zigzag.i = mbs_q_compd[mbn].i;
        mb_zigzag.s = mbw*mbh;
        mb_zigzag.arr = new uchar[mb_zigzag.s];
        
        for(int arrn=0; arrn<mbw*mbh;){
            
            if ( currDiag < mbw ){ // if doing the upper-left triangular half
                loopFrom = 0;
                loopTo = currDiag;
            }
            else{ // doing the bottom-right triangular half
                loopFrom = currDiag - mbw + 1;
                loopTo = mbw - 1;
            }
            
            for ( int i = loopFrom; i <= loopTo; i++ ){
                if ( currDiag % 2 == 0 ){ // want to fill upwards
                    row = loopTo - i + loopFrom;
                    col = i;
                }
                else{ // want to fill downwards
                    row = i;
                    col = loopTo - i + loopFrom;
                }
                mb_zigzag.arr[arrn] = mbs_q_compd[mbn].m.at<int8_t>(row, col);
                arrn++;
            }
            currDiag++;
        }
        mbs_zigzag.push_back(mb_zigzag);
    }
    
    return mbs_zigzag;
}

/* huffman coding */
vector<iArr> encode_huffman(vector<iArr> mbs_zigzag){
    vector<iArr> mbs_coded;
    
    uchar *mbs_arr = new uchar [mbh*mbw*mbs_zigzag.size()];
    
    for(int i=0; i<mbs_zigzag.size(); i++){
        
        iArr mb_coded;
        mb_coded.i = mbs_zigzag[i].i;
        mbs_coded.push_back(mb_coded);
        
        uchar *dst = &mbs_arr[i*mbh*mbw];
        memcpy(dst, mbs_zigzag[i].arr, mbh*mbw*sizeof(uchar));
    }
    
    if(mbs_coded.size()>0){
        iArr frame_coded;
        //huffman_encode(mbs_arr, mbs_zigzag.size()*mbh*mbw , &frame_coded.arr, &frame_coded.s);
        //delete[] frame_coded.arr;
        huffman_encode_canonical(mbs_arr, mbs_zigzag.size()*mbh*mbw , &frame_coded.arr, &frame_coded.s);
        mbs_coded[0].s = frame_coded.s;
        mbs_coded[0].arr = new uchar[frame_coded.s];
        memcpy(mbs_coded[0].arr, frame_coded.arr, frame_coded.s);
    }
    
    delete mbs_arr;
    return mbs_coded;
}

/* making the packet */
vector<uchar> pack_it(vector<iArr> mbs_coded){
    vector<uchar> packet;
    
    uint32_t start = 0x00000AB3; //packet header
    for(int t=0; t<4; t++){
        uchar byte = start>>8*t;
        packet.push_back(byte);
    }
    
    uint32_t no_mbs = mbs_coded.size(); //number of Micro Blocks
    for(int t=0; t<4; t++){
        uchar byte = no_mbs>>8*t;
        packet.push_back(byte);
    }
    
    for (int32_t i=0; i<no_mbs; i++){
        
        uint16_t mb_pos = mbs_coded[i].i; //Micro Block's position
        for(int t=0; t<2; t++){
            uchar byte = mb_pos>>8*t;
            packet.push_back(byte);
        }
    }
    
    if (mbs_coded.size()>0){
        
        uint32_t no_huff = mbs_coded[0].s; //Length of Huffman codes
        for(int t=0; t<4; t++){
            uchar byte = no_huff>>8*t;
            packet.push_back(byte);
        }
        
        for(int j=0; j<no_huff; j++){ //Huffman codes
            uchar byte = mbs_coded[0].arr[j];
            packet.push_back(byte);
        }
        
    }
    
    return packet;
}

/* encoder */
vector<uchar> encoder_job(Mat frame){
    Mat frame_f;
    frame.convertTo(frame_f, CV_32FC1, 1/255.0);
    vector<Mat> mbs = get_mbs(frame_f);
    vector<Mat> mbs_dct = get_DCT(mbs);
    vector<Mat> mbs_q = get_quant(mbs_dct);
    vector<iMat> mbs_q_compd = compare_mbs(mbs_q);
    vector<iArr> mbs_zigzag = to_zigzag(mbs_q_compd);
    vector<iArr> mbs_coded = encode_huffman(mbs_zigzag);
    vector<uchar> packet = pack_it(mbs_coded);
    return packet;
}

