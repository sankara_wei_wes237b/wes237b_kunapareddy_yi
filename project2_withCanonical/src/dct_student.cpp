#include "main.h"
#include <stdio.h>
#include "dct_student.h"
#include <math.h>
#define PI 3.1415926

float cosines[8][8] =
   {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0}};

float coef[8][8] =
   {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0}};

void init_dct(void) {
	float ci, cj;
	for (int i=0; i<8; i++) {
		ci=i? 1.0:sqrt(0.5);
		for (int j=0; j<8; j++) {
			cj=j?1.0:sqrt(0.5);
			cosines[i][j]=cos((2*i+1)*j*PI/16);
			coef[i][j] = ci*cj;
		}
	}
}




void DCT_student(Mat input, Mat output)
{
	float temp = 0.0;
	//output = Mat(8, 8, CV_32FC1);
	
	for (int i=0; i<8; i++) {
		for(int j=0; j<8; j++) {
			temp = 0.0;
			for(int x=0; x<8; x++) {
				for(int y=0; y<8; y++) {
					temp+= cosines[x][i]*cosines[y][j]*input.at<float>(x,y);
				}
			}
			temp*=coef[i][j]/4.0;
			output.at<float>(i,j)=temp;
		}
	}
}


void iDCT_student(Mat input, Mat output)
{
	float temp = 0.0;
	//output = Mat(8, 8, CV_32FC1);
	
	for (int x=0; x<8; x++) {
		for(int y=0; y<8; y++) {
			temp = 0.0;
			for(int i=0; i<8; i++) {
				for(int j=0; j<8; j++) {
					temp+= coef[i][j]*cosines[x][i]*cosines[y][j]*input.at<float>(i,j)/4.0;
				}
			}
			
			output.at<float>(x,y)=temp;
		}
	}
}
