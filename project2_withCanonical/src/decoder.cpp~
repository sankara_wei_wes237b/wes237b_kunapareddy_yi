//
//  decoder.cpp
//  Lab2
//
//  Created by Alireza on 7/6/16.
//  Copyright © 2016 Alireza. All rights reserved.
//

#include "main.h"
#include "huffman_canonical.h"
#include "dct_student.h"

Mat quanMat_de;
Mat frame_ref;
int width_ratio;

/* decoder initializer */
void decoder_init(){
    
    /* mpeg 1 quntizaton matrix */
    
    float q_arr[8][8] =
    {{8 , 16, 19, 22, 26, 27, 29, 34},
        {16, 16, 22, 24, 27, 29, 34, 37},
        {19, 22, 26, 27, 29, 34, 34, 38},
        {22, 22, 26, 27, 29, 34, 37, 40},
        {22, 26, 27, 29, 32, 35, 40, 48},
        {26, 27, 29, 32, 35, 40, 48, 58},
        {26, 27, 29, 34, 38, 46, 56, 69},
        {27, 29, 35, 38, 46, 56, 69, 83}};
    
    frame_ref = Mat::zeros(HEIGHT, WIDTH, CV_32FC1);
    quanMat_de = Mat(mbh, mbw, CV_32FC1);
    for(int r=0;r<8;r++){
        for(int c=0;c<8;c++){
            quanMat_de.at<float>(r,c) = q_arr[r][c]/127.0;
        }
    }
    width_ratio = WIDTH/mbw;
    init_dct();
    
}

/* unpacking */
vector<iArr> unpack_it(vector<uchar> packet){
    vector<iArr> mbs_coded;
    uint32_t p=0;
    uint32_t t;
    
    uint32_t start = 0;
    while((start != 0x00000AB3) && (p<packet.size())){ //search for packet header
        uchar byte = packet[p];
        p++;
        start = (start >> 8) + (byte << 24);
    }
    
    uint32_t no_mbs = 0; //number of Micro Blocks
    t=0;
    while((t<4) && (p<packet.size())){
        uchar byte = packet[p];
        p++;
        no_mbs = (no_mbs >> 8) + (byte << 24);
        t++;
    }
    
    for (int32_t i=0; i<no_mbs; i++){
        
        iArr mb_coded;
        
        uint16_t mb_pos = 0; //Micro Block's position
        t=0;
        while((t<2) && (p<packet.size())){
            uchar byte = packet[p];
            p++;
            mb_pos = (mb_pos >> 8) + (byte << 8);
            t++;
        }
        
        mb_coded.i = mb_pos;
        
        mbs_coded.push_back(mb_coded);
    }
    
    if (mbs_coded.size()>0){
        uint32_t no_huff = 0; //Length of Huffman codes
        t=0;
        while((t<4) && (p<packet.size())){
            uchar byte = packet[p];
            p++;
            no_huff = (no_huff >> 8) + (byte << 24);
            t++;
        }
        mbs_coded[0].s=no_huff;
        mbs_coded[0].arr = new uchar[no_huff];
        
        t=0;
        while((t<no_huff) && (p<packet.size())){ //Huffman codes
            uchar byte = packet[p];
            p++;
            mbs_coded[0].arr[t] = byte;
            t++;
        }
    }
    
    return mbs_coded;
}

/* huffman decoder */
vector<iArr> decode_huffman(vector<iArr> mbs_coded){
    vector<iArr> mbs_zigzag;
    
    uchar *mbs_arr;
    uint32_t mbs_arr_size;
    
    if(mbs_coded.size()>0){
        //huffman_decode(mbs_coded[0].arr, mbs_coded[0].s , &mbs_arr, &mbs_arr_size);

        delete[] mbs_arr;
        huffman_decode_canonical(mbs_coded[0].arr, mbs_coded[0].s , &mbs_arr, &mbs_arr_size);
    }
    
    for(int i=0; i<mbs_coded.size(); i++){
        iArr mb_zigzag;
        mb_zigzag.i = mbs_coded[i].i;
        mb_zigzag.arr = new uchar[mbh*mbw];
        memcpy(mb_zigzag.arr, &mbs_arr[i*mbh*mbw], mbw*mbh*sizeof(uchar));
        mbs_zigzag.push_back(mb_zigzag);
    }
    
    return mbs_zigzag;
}

/* from zigzag */
vector<iMat> fromzigzag(vector<iArr> mbs_zigzag){
    vector<iMat> mbs_q_compd;
    
    for(int mbn=0; mbn<mbs_zigzag.size(); mbn++){
        
        int currDiag = 0;
        int loopFrom;
        int loopTo;
        int row;
        int col;
        
        iMat mb_q_compd;
        mb_q_compd.i = mbs_zigzag[mbn].i;
        mb_q_compd.m = Mat::zeros(mbh, mbw, CV_8SC1);
        
        for(int arrn=0; arrn<sizeof(mbs_zigzag[mbn].arr);){
            
            if ( currDiag < mbw ){ // if doing the upper-left triangular half
                loopFrom = 0;
                loopTo = currDiag;
            }
            else{ // doing the bottom-right triangular half
                loopFrom = currDiag - mbw + 1;
                loopTo = mbw - 1;
            }
            
            for ( int i = loopFrom; i <= loopTo; i++ ){
                if ( currDiag % 2 == 0 ){ // want to fill upwards
                    row = loopTo - i + loopFrom;
                    col = i;
                }
                else{ // want to fill downwards
                    row = i;
                    col = loopTo - i + loopFrom;
                }
                mb_q_compd.m.at<int8_t>(row,col) = mbs_zigzag[mbn].arr[arrn];
                arrn++;
            }
            currDiag++;
        }
        mbs_q_compd.push_back(mb_q_compd);
    }
    
    return mbs_q_compd;
}

/* dequantization */
vector<iMat> get_dequant(vector<iMat> mbs_q_compd){
    vector<iMat> mbs_dct_compd;
    for(int i=0; i<mbs_q_compd.size(); i++){
        iMat mb_dct_compd;
        mb_dct_compd.i = mbs_q_compd[i].i;
        Mat temp = mbs_q_compd[i].m;
        
        /* converting to 32 bits float */
        temp.convertTo(temp, CV_32FC1);
        multiply(temp, quanMat_de, mb_dct_compd.m);
        mbs_dct_compd.push_back(mb_dct_compd);
    }
    return mbs_dct_compd;
};

/* IDCT */
vector<iMat> get_IDCT(vector<iMat> mbs_dct_compd){
    vector<iMat> mbs_compd;
    for(int i=0; i<mbs_dct_compd.size(); i++){
        iMat mb_compd;
        mb_compd.i = mbs_dct_compd[i].i;
        //idct(mbs_dct_compd[i].m, mb_compd.m);
        mb_compd.m = Mat(8, 8, CV_32FC1);
        iDCT_student(mbs_dct_compd[i].m, mb_compd.m);
        mbs_compd.push_back(mb_compd);
    }
    return mbs_compd;
};

/* merging micro blocks */
Mat get_frame(vector<iMat> mbs_compd){
    Mat frame = frame_ref.clone();
    for(int i=0; i<mbs_compd.size(); i++){
        int I = mbs_compd[i].i;
        int I_ratio = I/width_ratio;
        int r = I_ratio*mbh;
        int c = (I - I_ratio*width_ratio)*mbw;
        mbs_compd[i].m.copyTo(frame(Rect(c, r, mbw, mbh)));
    }
    frame.copyTo(frame_ref);
    return frame;
};

/* decoder */
Mat decoder_job(vector<uchar> packet){
    vector<iArr> mbs_coded = unpack_it(packet);
    vector<iArr> mbs_zigzag = decode_huffman(mbs_coded);
    vector<iMat> mbs_q_compd = fromzigzag(mbs_zigzag);
    vector<iMat> mbs_dct_compd = get_dequant(mbs_q_compd);
    vector<iMat> mbs_compd = get_IDCT(mbs_dct_compd);
    Mat frame_f = get_frame(mbs_compd);
    Mat frame;
    frame_f.convertTo(frame, CV_8UC1, 255);
    return frame;
}
