#include "main.h"
#include <stdio.h>




void init_dct(void);
void init_dct2(void);
void init_dct2_encode(void);

void DCT_student(Mat input, Mat output);
void iDCT_student(Mat input, Mat output);
void DCT2_student(Mat input, Mat output);
void iDCT2_student(Mat input, Mat output);
void DCT2_quant_student(Mat input, Mat output);
void iDCT2_dequant_student(Mat input, Mat output);
