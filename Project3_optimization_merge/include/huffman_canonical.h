
#ifndef HUFFMAN_H
#define HUFFMAN_H

#include <stdio.h>
#include <stdint.h>

#define MAX_CHAR 256

		   
int huffman_encode_canonical(const unsigned char *bufin,
           uint32_t bufinlen,
           unsigned char **pbufout,
           uint32_t *pbufoutlen);
		   
int huffman_decode_canonical(const unsigned char *bufin,
           uint32_t bufinlen,
           unsigned char **bufout,
           uint32_t *pbufoutlen);


//type def for struct
typedef struct node huffnode;
typedef struct encode ecode;
struct node {
	int freq;
	char name;
	huffnode *left;
	huffnode *right;
};

struct encode {
	int code;
	int len;
};

//external variable declaration
extern huffnode first_queue[256];  //global variable to store tree nodes
extern huffnode second_queue[256]; //global variable to store tree nodes
extern int first_p1, first_p2, second_p1,second_p2; //global variable as pointers for first_queue and second queue



void getFreqArr(const unsigned char *in, int len, int freq[]);  
unsigned int generateTreeArr(unsigned char *out, int freq[]);  //write tree info to char array, return index for writing actual data
unsigned int readTreeArr(const unsigned char *inf, int freq[]); //read array to retrieve the tree, return index of actual data




huffnode *buildtree(int freq[]); //build the tree based on the freq array, return the pointer of tree root

void createcode(huffnode *root, ecode code[]); //generate encoding table based on the tree


unsigned int encodeArr(const unsigned char *in, int len, unsigned char *out, int start, ecode code[]); //generate encoded data based on ecode array, return length of output array
unsigned int decodeArr(const unsigned char *in, int len, int start, unsigned char *out, huffnode* root); //decode input based on huffman tree, return length of output array

//new function for canonical huffman

void convert_canonical_code(ecode code[]);	//convert to canonical code
unsigned int generateCanHeader(unsigned char *out, int freq[]);  //write canonical header to out buf. (char and len pair)
unsigned int encodeArr_Can(const unsigned char *in, int len, unsigned char *out, int start, ecode code[]); //generate encoded data based on ecode array, return length of output array
unsigned int decodeArr_Can(const unsigned char *in, int len, unsigned char *out);

 
#endif
