//
//  main.h
//  Lab2
//
//  Created by Alireza on 7/6/16.
//  Copyright © 2016 Alireza. All rights reserved.
//

#ifndef main_h
#define main_h

#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

#define mbh     8
#define mbw     8

#define	CAM	1

#if CAM
#define INPUT	0
#define WIDTH   320
#define HEIGHT  256
#else
#define INPUT	"/root/Desktop/Lab_2/input.raw"
#define WIDTH   720
#define HEIGHT  480
#endif

struct iMat{
    uint16_t i;
    Mat m;
};

struct iArr{
    uint16_t i;
    uchar* arr;
    uint32_t s;

    iArr(){
	arr = NULL;
	s = 0;
    }
};

/* encoder */
void encoder_init(void);
vector<uchar> encoder_job(Mat frame);

/* decoder */
void decoder_init(void);
Mat decoder_job(vector<uchar> packet);

#endif /* main_h */
