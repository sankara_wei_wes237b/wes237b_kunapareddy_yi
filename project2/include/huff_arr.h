#include <stdio.h>
#define MAX_CHAR 256

//type def for struct
typedef struct node huffnode;
typedef struct encode ecode;
struct node {
	int freq;
	char name;
	huffnode *left;
	huffnode *right;
};

struct encode {
	int code;
	int len;
};

//external variable declaration
extern huffnode first_queue[256];  //global variable to store tree nodes
extern huffnode second_queue[256]; //global variable to store tree nodes
extern int first_p1, first_p2, second_p1,second_p2; //global variable as pointers for first_queue and second queue

//functions declaration
//void getFreq(FILE *inf, int freq[]);  //go through input file to retrieve freq of each character, save to array of size 256
//void generateTreeFile(FILE *outf, int freq[]);
//void readTreeFile(FILE *inf, int freq[]);

//similiar functions as above but deal with char array instead of file
void getFreqArr(const unsigned char *in, int len, int freq[]);  
unsigned int generateTreeArr(unsigned char *out, int freq[]);  //write tree info to char array, return index for writing actual data
unsigned int readTreeArr(const unsigned char *inf, int freq[]); //read array to retrieve the tree, return index of actual data




huffnode *buildtree(int freq[]); //build the tree based on the freq array, return the pointer of tree root

void createcode(huffnode *root, ecode code[]); //generate encoding table based on the tree
void encodeFile(FILE *inf, FILE *outf, ecode code[]); //generate encoded file based on ecode array
void decodeFile(FILE *inf, FILE *outf, huffnode* root); //decode file based on huffman tree

//similiar functions as above but deal with array instead of file
unsigned int encodeArr(const unsigned char *in, int len, unsigned char *out, int start, ecode code[]); //generate encoded data based on ecode array, return length of output array
unsigned int decodeArr(const unsigned char *in, int len, int start, unsigned char *out, huffnode* root); //decode input based on huffman tree, return length of output array
