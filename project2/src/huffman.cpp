#include "huffman.h"

#include <stdio.h>
#include "huff_arr.h"
#include <string.h>

#define MAXSIZE 345600
unsigned char decoded[MAXSIZE];
unsigned char encoded[MAXSIZE];

int freq[MAX_CHAR]; //storing frequency for each character
ecode code_map[MAX_CHAR];
huffnode *t_root;


int huffman_encode(const unsigned char *bufin,
		   uint32_t bufinlen,
		   unsigned char **pbufout,
		   uint32_t *pbufoutlen) 
{
	getFreqArr(bufin, bufinlen, freq); //go through input array and update freq array
    *pbufout=encoded;

	unsigned int start=generateTreeArr(*pbufout, freq);  
	
	t_root=buildtree(freq);
	createcode(t_root, code_map);
	*pbufoutlen = encodeArr(bufin, bufinlen, *pbufout, start, code_map); //generate encoded data based on ecode array, return length of output array
	return 0;
}

int huffman_decode(const unsigned char *bufin,
  		   uint32_t bufinlen,
		   unsigned char **bufout,
		   uint32_t *pbufoutlen)
{
		
		unsigned int start = readTreeArr(bufin, freq);
		t_root=buildtree(freq);
                *bufout = decoded;
		
		*pbufoutlen=decodeArr(bufin, bufinlen, start, *bufout, t_root);
		return 0;

}
