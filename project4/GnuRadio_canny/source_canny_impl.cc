/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "source_canny_impl.h"

#include "main.h"


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
using namespace cv;
using namespace std;
namespace gr {
  namespace vision {
    cv::VideoCapture cap2;
    source_canny::sptr
    source_canny::make()
    {
      
      cap2.open(0);
      if(!cap2.isOpened())
	cap2.open(1);
      if(!cap2.isOpened())
	printf("cannot open video0 or video1\n");
      cap2.set(CV_CAP_PROP_FRAME_WIDTH, 160);
      cap2.set(CV_CAP_PROP_FRAME_HEIGHT, 120);




      return gnuradio::get_initial_sptr
        (new source_canny_impl());
    }

    /*
     * The private constructor
     */
    source_canny_impl::source_canny_impl()
      : gr::block("source_canny",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1, sizeof(unsigned char)))
    {}

    /*
     * Our virtual destructor.
     */
    source_canny_impl::~source_canny_impl()
    {
    }

    void
    source_canny_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
        /* <+forecast+> e.g. ninput_items_required[0] = noutput_items */
    }

    int
    source_canny_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
        unsigned char *out = (unsigned char *) output_items[0];
	cv::Mat frame, gray;
        cv::Mat canny_out, sobel_out, sobel_angle;
        cv::Mat g_out, test_out;
	cap2 >> frame;
	cv::cvtColor(frame, gray, CV_BGR2GRAY);

	
	cv::resize(gray,gray,Size(WIDTH,HEIGHT));
        gray.convertTo(gray,CV_32FC1, 1);


	//init for gaussian denoise and sobel filter

	sobel_out = Mat(HEIGHT, WIDTH, CV_32FC1);
	sobel_angle = Mat(HEIGHT, WIDTH, CV_32FC1); //possible value 0: 0 degree; 1: 45 degree; 2: 90 degree
	g_out = Mat(HEIGHT, WIDTH, CV_32FC1);
        int test1 = TEST;
        std::cout << "test value: "<<test1<<endl;
	canny_out = Mat(HEIGHT, WIDTH, CV_32FC1);

	student_gaussian_denoise(gray, &g_out);
	student_sobel_angle(g_out, &sobel_out, &sobel_angle);

	student_nonMax_sup(sobel_out, canny_out, sobel_angle);
	student_double_thresh(canny_out, sobel_angle, 0.2, 0.4);

        //canny_out.convertTo(canny_out,CV_32FC1, 255.0);
	canny_out.convertTo(test_out,CV_8U, 1);
	
	int r,c;

	for(r=0; r<test_out.rows; r++)
	{
  	  unsigned char *pix = test_out.ptr<uchar>(r);
	  for(c=0; c<test_out.cols; c++)
 	  {
	    *out = pix[c];
	    out++;
	  }
	}

        // Do <+signal processing+>
        // Tell runtime system how many input items we consumed on
        // each input stream.
        consume_each (noutput_items);

        // Tell runtime system how many output items we produced.
        return test_out.cols*test_out.rows;
    }

  } /* namespace vision */
} /* namespace gr */

