#ifndef main_h
#define main_h

#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

#define	CAM	1

#if CAM
#define INPUT	0
#define WIDTH   160 //320
#define HEIGHT  120 //256
#else
#define INPUT	"input.raw"
#define WIDTH   720
#define HEIGHT  480
#endif

#define TEST 888
//void student_sobel(Mat in, Mat* out);
void student_gaussian_denoise(Mat in, Mat* out);
void kernel_scale(void);
void student_sobel_angle(Mat in, Mat* out, Mat* angle);
void student_nonMax_sup(Mat &sobel_out, Mat &c_out, Mat &sobel_angle);
void student_double_thresh(Mat &in, Mat &angle, float low, float high);

#endif /* main_h */
