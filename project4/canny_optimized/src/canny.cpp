#include "main.h"
#include <stdio.h>

#include "time.h"

int main(int argc, const char * argv[]) {
    
    cout << "WES237B lab 3\n";
    //test_neon_angle(); //testing only
    //test_neon_nonMax();  //testing only

    VideoCapture cap(INPUT);

    Mat frame, gray, canny_out, sobel_out, sobel_angle, g_out;

	//init for gaussian denoise and sobel filter
	sobel_out = Mat(HEIGHT, WIDTH, CV_32FC1, 0.0);
	sobel_angle = Mat(HEIGHT, WIDTH, CV_32FC1); //possible value 0: 0 degree; 1: 45 degree; 2: 90 degree
	g_out = Mat(HEIGHT, WIDTH, CV_32FC1, 0.0);
	canny_out = Mat(HEIGHT, WIDTH, CV_32FC1, 0.0);
	kernel_scale();

    char key=0;

    time_t timer;
    
    time_t start;
    time(&start);
    int fps_cnt = 0;
       
    while(key != 'q'){
        
        cap >> frame;
	if(frame.empty()){
	    break;
	}
 
        
        time(&timer);
        if(timer - start > 1l) {
            start = timer;
            std::cout << "fps: " << fps_cnt << endl;
            fps_cnt = 0;
        }
        fps_cnt++;   

	cvtColor(frame, gray, CV_BGR2GRAY);
	resize(gray,gray,Size(WIDTH,HEIGHT));
    gray.convertTo(gray,CV_32FC1, 1/255.0);


	student_gaussian_denoise(gray, &g_out);
	student_sobel_angle(g_out, &sobel_out, &sobel_angle);

	student_nonMax_sup(sobel_out, canny_out, sobel_angle);
	student_double_thresh(canny_out, sobel_angle, 0.2, 0.4);

	//show original gray image
	//show sobel result

     imshow("original", gray);
	 //imshow("gaussian", g_out);
     imshow("canny", canny_out);
     //key=waitKey
     key = waitKey(1);
     //key = 'q';

        time(&timer);
        
    }

    return 0;
}

