#include "main.h"
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <arm_neon.h>

#define PI 3.1415926
#define TAN225 0.4142
#define TAN675 2.4142


float kernel[5][5] __attribute__((aligned(0x10)))= {{2, 4, 5, 4, 2},
													{4, 9, 12, 9, 4},
													{5, 12, 15, 12, 5},
													{4, 9, 12, 9, 4},
													{2, 4, 5, 4, 2}};
float kernelx[3][3] __attribute__((aligned(0x10)))= {{-1, 0, 1},
													{-2, 0, 2},
													{-1, 0, 1}};
float kernely[3][3] __attribute__((aligned(0x10)))= {{-1, -2, -1},
													{0, 0, 0},
													{1, 2, 1}};

inline void neon_angle(float *angle, int32x4_t &i, int32x4_t &j){
    i = vdupq_n_s32(0);
	j = vdupq_n_s32(0);
	float32x4_t tan225, tan675, neg_tan225, neg_tan675, v_angle;
	int32x4_t temp_s;
	uint32x4_t temp_u;
	
	tan225 = vdupq_n_f32(TAN225);
	tan675 = vdupq_n_f32(TAN675);
	neg_tan225 = vnegq_f32(tan225);
	neg_tan675 = vnegq_f32(tan675);
	v_angle = vld1q_f32(angle);

	temp_u = vorrq_u32(vcgtq_f32(v_angle, tan675), vcltq_f32(v_angle, neg_tan675)); //check if angle greater than 67.5 or less than -67.5
	temp_s = vreinterpretq_s32_u32(temp_u); // temp_s will be -1 if above statement is true, will be 0 if false
	i = vaddq_s32(i, temp_s);  // i=-1; j=0
	
	temp_u = vandq_u32(vcgtq_f32(v_angle, tan225), vcleq_f32(v_angle, tan675)); //check if angle between 22.5 - 67.5
	temp_s = vreinterpretq_s32_u32(temp_u); // temp_s will be -1 if above statement is true, will be 0 if false
	i = vaddq_s32(i, temp_s);	// i=-1; j=-1;
	j = vaddq_s32(j, temp_s);
	
	temp_u = vandq_u32(vcgtq_f32(v_angle, neg_tan225), vcleq_f32(v_angle, tan225)); //check if angle between -22.5 and 22.5
	temp_s = vreinterpretq_s32_u32(temp_u); // temp_s will be -1 if above statement is true, will be 0 if false
	j = vaddq_s32(j, temp_s);   // i=0; j=-1

	temp_u = vandq_u32(vcgtq_f32(v_angle, neg_tan675), vcleq_f32(v_angle, neg_tan225)); //check if angle between -67.5 and -22.5
	temp_s = vreinterpretq_s32_u32(temp_u); // temp_s will be -1 if above statement is true, will be 0 if false
	i = vaddq_s32(i, temp_s);	//i=-1; j=1
	j = vsubq_s32(j, temp_s);	
	
}

inline void neon_noMax_sup(float *in, float *out, float *angle, int mat_width) {
    uint32x4_t temp_u;
	int32x4_t temp_s;
	float32x4_t input, temp_f;
	float32x4_t *output;
	
	input = vld1q_f32(in);
	output = (float32x4_t *) out; 
	
	union {
    int32x4_t vec;
    int32_t v[4];
    } i_off, j_off;

	union {
	float32x4_t acc;
	float f[4];
	} l,h;
	neon_angle(angle, i_off.vec, j_off.vec);
	l.f[0] = *(in + mat_width * i_off.v[0] + j_off.v[0]);
	h.f[0] = *(in - mat_width * i_off.v[0] - j_off.v[0]);
	l.f[1] = *(in + mat_width * i_off.v[1] + j_off.v[1] +1);
	h.f[1] = *(in - mat_width * i_off.v[1] - j_off.v[1] +1);
	l.f[2] = *(in + mat_width * i_off.v[2] + j_off.v[2] +2);
	h.f[2] = *(in - mat_width * i_off.v[2] - j_off.v[2] +2);
	l.f[3] = *(in + mat_width * i_off.v[3] + j_off.v[3] +3);
	h.f[3] = *(in - mat_width * i_off.v[3] - j_off.v[3] +3);

        //std::cout << "l value: " << l.f[0] << " " << l.f[1] << " " << l.f[2]<< " " << l.f[3] << endl;
        //std::cout << "h value: " << h.f[0] << " " << h.f[1] << " " << h.f[2]<< " " << h.f[3] << endl;


	temp_u = vandq_u32(vcgeq_f32(input, l.acc), vcgtq_f32(input, h.acc)); //comparing the neighbor, if max, return all 1's
	temp_s = vreinterpretq_s32_u32(temp_u);
	temp_f = vcvtq_f32_s32(temp_s);
	temp_f = vnegq_f32(temp_f);
	*output = vmulq_f32(input, temp_f);
	
}



void test_neon_nonMax(void){
    float q_arr[8][8] =
	{{140,144,147,140,140,155,179,175},
	{144,152,140,147,140,148,167,179},
	{152,155,136,167,163,162,152,172},
	{168,145,156,160,152,155,136,160},
	{162,148,156,148,140,136,147,162},
	{147,167,140,155,155,140,136,162},
	{136,156,123,167,162,144,140,147},
	{148,155,136,155,152,147,147,136}};
   float out[4] = {0,0,0,0};
   float angle[4] = {0.2, 1.1, 9.6, -15.1};
   float *in = &q_arr[5][2];
   neon_noMax_sup(in, out, angle, 8);
   for(int i=0; i<4; i++)
       std::cout << out[i] << endl;


}
void test_neon_angle(void){
    float angle[4] = {0.2, 1.1, 9.6, -15.1};
    union {
    int32x4_t vec;
    int32_t v[4];
    } i,j;
    neon_angle(angle, i.vec, j.vec);
    for (int t=0; t<4; t++){
        std::cout << "i " << i.v[t] << "; j " << j.v[t] << endl;
    }

}


void kernel_scale(void){
	for (int i=0; i<5; i++){
		//for(int j=0; j<5; j++)
			//kernel[i][j] /= 159;
		kernel[i][0] /= 159;
		kernel[i][1] /= 159;
		kernel[i][2] /= 159;
		kernel[i][3] /= 159;
		kernel[i][4] /= 159;
	}
}


void student_gaussian_denoise(Mat in, Mat* out){

float32x4_t temp, k;
#pragma omp parallel
	//*out = Mat(HEIGHT, WIDTH, CV_32FC1);

	for (int x=2; x<HEIGHT-2; x++){
#pragma omp for 
		for(int y=2; y<WIDTH-2; y=y+4) {
			//out->at<float>(x, y) = 0;
                        temp = vdupq_n_f32(0.f);
			#pragma omp for 
			for(int off_row=-2; off_row<=2; off_row++) {
				#pragma omp for 
				for(int off_col=-2; off_col<=2; off_col++){
					//out->at<float>(x,y) += in.at<float>(x+off_row, y+off_col) * kernel[off_row+2][off_col+2];
                                        k = vdupq_n_f32(kernel[off_row+2][off_col+2]);
                                        temp = vmlaq_f32(temp, vld1q_f32(&in.at<float>(x+off_row, y+off_col)), k);
				}
			}
                        
                        float32x4_t *p = (float32x4_t *)&(out->at<float>(x,y));
                        *p = temp;
		}
	}

}

void student_sobel_angle(Mat in, Mat* out, Mat* angle){

#pragma omp parallel
    //*out = Mat(HEIGHT, WIDTH, CV_32FC1);
//static int ite =0;
//std::cout << "start_soble" << endl; ite++;
	float32x4_t Gx, Gy, G, two;
        two = vdupq_n_f32(2.f);
    for (int i=2; i<HEIGHT-2; i++) {
        for (int j=2; j<WIDTH-2; j=j+4) {
            Gx = vdupq_n_f32(0.f);
			#pragma omp for 
            Gy = vdupq_n_f32(0.f);
          
            
            Gy = vsubq_f32(Gy, vld1q_f32(&in.at<float>(i-1,j-1)));
            Gy = vmlsq_f32(Gy, vld1q_f32(&in.at<float>(i-1,j)), two);
            Gy = vsubq_f32(Gy, vld1q_f32(&in.at<float>(i-1,j+1)));
            Gy = vaddq_f32(Gy, vld1q_f32(&in.at<float>(i+1,j-1)));
            Gy = vmlaq_f32(Gy, vld1q_f32(&in.at<float>(i+1,j)), two);
            Gy = vaddq_f32(Gy, vld1q_f32(&in.at<float>(i+1,j+1)));

            Gx = vsubq_f32(Gx, vld1q_f32(&in.at<float>(i-1,j-1)));
            Gx = vaddq_f32(Gx, vld1q_f32(&in.at<float>(i-1,j+1)));
            Gx = vmlsq_f32(Gx, vld1q_f32(&in.at<float>(i,j-1)), two);
            Gx = vmlaq_f32(Gx, vld1q_f32(&in.at<float>(i,j+1)), two);
            Gx = vsubq_f32(Gx, vld1q_f32(&in.at<float>(i+1,j-1)));
            Gx = vaddq_f32(Gx, vld1q_f32(&in.at<float>(i+1,j+1)));
//std::cout << "complete Gx and Gy" << endl;
            G = vaddq_f32(vmulq_f32(Gx, Gx), vmulq_f32(Gy,Gy));
            G = vrecpeq_f32(vrsqrteq_f32(G));
            float32x4_t *p = (float32x4_t *)&(out->at<float>(i,j));
            *p = G;
            float32x4_t *p1 = (float32x4_t *)&(angle->at<float>(i,j));
            *p1 = vmulq_f32(Gy, vrecpeq_f32(Gx));
//std::cout << "round " <<ite <<"; i " << i << "; j " << j << endl;
/*
            for(int off_row=-1; off_row<2; off_row++) {
				#pragma omp for 
                for (int off_col=-1; off_col<2; off_col++) {
                    Gx += in.at<float>(i+off_row, j+off_col) * kernelx[off_row+1][off_col+1];
                    Gy += in.at<float>(i+off_row, j+off_col) * kernely[off_row+1][off_col+1];
                }
            }

*/
 /*           Gy -= in.at<float>(i-1, j-1);
            Gy -= (in.at<float>(i-1, j) + in.at<float>(i-1, j));
            Gy -= in.at<float>(i-1, j+1);
            Gy += in.at<float>(i+1, j-1);
            Gy += (in.at<float>(i+1, j) + in.at<float>(i+1, j));
            Gy += in.at<float>(i+1, j+1);

            Gx -= in.at<float>(i-1, j-1);
            Gx += in.at<float>(i-1, j+1);
            Gx -= (in.at<float>(i, j-1) + in.at<float>(i, j-1));
            Gx += (in.at<float>(i, j+1) + in.at<float>(i, j+1));
            Gx -= in.at<float>(i+1, j-1);
            Gx += in.at<float>(i+1, j+1);

            G = Gx * Gx + Gy * Gy;
            G = sqrtf(G);
*/

            //out->at<float>(i,j) = G; //sqrt(Gx*Gx + Gy*Gy);

            //angle->at<float>(i,j) = (float) atan(Gy/Gx);
            //angle->at<float>(i,j) = Gy/Gx;
        }
    }

}

void student_nonMax_sup(Mat &sobel_out, Mat &c_out, Mat &sobel_angle) {
	float l,h, l2, h2;
	#pragma omp parallel
	for (int i=2; i<HEIGHT-2; i++){
		#pragma omp parallel
		for (int j=2; j<WIDTH-2; j=j+4){
                     neon_noMax_sup(&sobel_out.at<float>(i,j), &c_out.at<float>(i,j), &sobel_angle.at<float>(i,j), WIDTH);




/*
			//if(sobel_angle.at<float>(i,j)> PI*3/4 || sobel_angle.at<float>(i,j)< -PI*3/4){
                        if(sobel_angle.at<float>(i,j)> TAN675 || sobel_angle.at<float>(i,j)< -TAN675){
				l=sobel_out.at<float>(i-1, j);
				h=sobel_out.at<float>(i+1, j);
				l2=sobel_out.at<float>(i-2, j);
				h2=sobel_out.at<float>(i+2, j);
			}
			//else if(sobel_angle.at<float>(i,j)> PI/4){
			else if(sobel_angle.at<float>(i,j)> TAN225){
				l=sobel_out.at<float>(i-1, j-1);
				h=sobel_out.at<float>(i+1, j+1);
				l2=sobel_out.at<float>(i-2, j-2);
				h2=sobel_out.at<float>(i+2, j+2);
			}
			//else if(sobel_angle.at<float>(i,j)> -PI/4){
			else if(sobel_angle.at<float>(i,j)> -TAN225){
				l=sobel_out.at<float>(i, j-1);
				h=sobel_out.at<float>(i, j+1);
				l2=sobel_out.at<float>(i, j-2);
				h2=sobel_out.at<float>(i, j+2);
			}
			else {
				l=sobel_out.at<float>(i-1, j+1);
				h=sobel_out.at<float>(i+1, j-1);
				l2=sobel_out.at<float>(i-2, j+2);
				h2=sobel_out.at<float>(i+2, j-2);
			}

			if(sobel_out.at<float>(i,j)<=l || sobel_out.at<float>(i,j)<=h || sobel_out.at<float>(i,j)<=l2 || sobel_out.at<float>(i,j)<=h2)
				c_out.at<float>(i,j) = 0;
			else
				c_out.at<float>(i,j) = sobel_out.at<float>(i,j);

*/
		}

	}
}
void student_double_thresh(Mat &in, Mat &sobel_angle, float low, float high){
	#pragma omp parallel
	for(int i=1; i<HEIGHT-1; i++){
		for(int j=1; j<WIDTH-1; j++){
			if(in.at<float>(i,j)<low){
				in.at<float>(i,j) = 0;
			}
			else if(in.at<float>(i,j)<high){
				float l, h;
				//if(sobel_angle.at<float>(i,j)> PI*3/4 || sobel_angle.at<float>(i,j)< -PI*3/4){
				if(sobel_angle.at<float>(i,j)> TAN675 || sobel_angle.at<float>(i,j)< -TAN675){
					l=in.at<float>(i, j-1);
					h=in.at<float>(i, j+1);
				}
				//else if(sobel_angle.at<float>(i,j)> PI/4){
				else if(sobel_angle.at<float>(i,j)> TAN225){
					l=in.at<float>(i-1, j+1);
					h=in.at<float>(i+1, j-1);
				}
				//else if(sobel_angle.at<float>(i,j)> -PI/4){
				else if(sobel_angle.at<float>(i,j)> -TAN225){
					l=in.at<float>(i-1, j);
					h=in.at<float>(i+1, j);
				}
				else {
					l=in.at<float>(i-1, j-1);
					h=in.at<float>(i+1, j+1);
				}
				if (h < high && l < high)
					in.at<float>(i,j) = 0;
				else
					in.at<float>(i,j) = high;

			}
		}
	}
}

