
#ifndef main_h
#define main_h

#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

#define	CAM	0

#if CAM
#define INPUT	0
#define WIDTH   320
#define HEIGHT  256
#else
#define INPUT	"/root/project/wes237b_kunapareddy_yi/Project3_Sobel/input.raw"
#define WIDTH   720
#define HEIGHT  480
#endif

void student_sobel(Mat in, Mat* out);
void student_gaussian_denoise(Mat in, Mat* out);

#endif /* main_h */
