#include "main.h"
#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    cout << "WES237B lab 4 canny openCV\n";

    VideoCapture cap(INPUT);

    Mat frame, gray, smooth, canny_out;
	float lowThreshold = 50;

	if (argc >=2) {
	    lowThreshold = atof(argv[1]);
		if (lowThreshold > 0.3)
		    lowThreshold = 0.3;
	}

    char key=0;
       
    while(key != 'q'){
        
        cap >> frame;
	if(frame.empty()){
	    break;
	}
    
	cvtColor(frame, gray, CV_BGR2GRAY);
	resize(gray,gray,Size(WIDTH,HEIGHT));
    //gray.convertTo(gray,CV_32FC1, 1/255.0);
	gray.convertTo(gray,CV_8U, 1);

	blur(gray, smooth, Size(5,5)); //size can be 3x3 in tutorial

	Canny( smooth, canny_out, lowThreshold, lowThreshold*2, 3 );



     imshow("original", gray);
     imshow("smooth", smooth);
     imshow("canny", canny_out);
     //key=waitKey
     key = waitKey(1);
     //key = 'q';
        
    }

    return 0;
}

